(function ($, DataTable) {
    "use strict";

    DataTable.ext.buttons.importExcel = {
        className: 'open-modal-dialog excel-modal',
        text: function (dt) {
            return '<i class="fa fa-upload"></i> ' + dt.i18n('buttons.import-excel', 'Import Excel');
        }

        /*action: function (e, dt, button, config) {
            window.location = window.location.href.replace(/\/+$/, "") + '/create';
        }*/
    };
})(jQuery, jQuery.fn.dataTable);