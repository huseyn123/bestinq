<?php

use Illuminate\Http\Request;


// patient routes
Route::get('/', 'UserController@show')->name('user.profile')->middleware('auth:web');
Route::get('/basket', 'UserController@basket')->name('user.basket')->middleware('auth:web');
Route::put('/profile/update/{id}', 'UserController@updateProfile')->name('user.update')->middleware('auth:web');
Route::post('/profile/passwordUpdate', 'UserController@updatePassword')->name('user.password.update')->middleware('auth:web');
