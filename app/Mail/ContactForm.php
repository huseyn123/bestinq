<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    public $content, $blade, $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content, $blade, $subject,$files = null)
    {
        $this->content = $content;
        $this->blade = $blade;
        $this->subject = $subject;
        $this->files = $files;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(isset($this->content['email'])){
            $replyTo = $this->content['email'];
        }
        else{
            $replyTo = config('mail.username');
        }

        $attach =  $this->replyTo($replyTo)
            ->subject($this->subject)
            ->markdown("emails.$this->blade")
            ->with('content',$this->content);

        if(!empty($this->files)){
            foreach ($this->files as $file) {
                $attach->attach($file, [
                    'as' => $file->getClientOriginalExtension(),
                    'mime' => $file->getMimeType()
                ]);
            }
        }
        return $attach;
    }

}
