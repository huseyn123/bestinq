<?php

namespace App\DataTables;

use App\Models\Page;
use App\Models\PageTranslation;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class CertificateDataTable extends DataTable
{

    protected $route;
    protected $album = false;



    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page',[
                    'album' => false,
                    'editRoute' => 'certificate',
                    'route' => 'page',
                    'row' => $row,
                    'softDelete' => true,
                    'forceDelete' => true,
                    'largeModal' => true,
                ])->render();
            })
            ->rawColumns(['visible', 'action', 'template_id', 'summary', 'filename','slug']);
    }



    public function query(Page $model)
    {
        $query = $model->newQuery()
            ->join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->leftJoin('page_translations as parent', 'parent.id', '=', 'pt.parent_id')
            ->where('pages.template_id',5)
            ->select(
                'pages.visible',
                'pages.template_id',
                'parent.name as parent',
                'pt.*',
                'pt.id as tid',
                'pages.id'
            );
        //->whereRaw('page_translations.id IN (SELECT id FROM page_translations WHERE `default` IN (SELECT MAX(`default`) FROM page_translations) )')



        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('pt.lang', $this->request()->get('lang'));
        }

        if($this->request()->get('type') == 1){
            $query->whereNull('pt.deleted_at');
        }
        elseif($this->request()->get('type') == 2){
            $query->whereNotNull('pt.deleted_at');
        }

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'pt.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'pt.name', 'title' => 'Ad'],
            ['data' => 'parent', 'name' => 'parent.name', 'title' => 'Kateqoriya'],
            ['data' => 'lang', 'name' => 'pt.lang', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'pt.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'pt.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25,50],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }
}
