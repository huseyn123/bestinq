<?php

namespace App\DataTables;

use App\Models\Certificates;
use Yajra\DataTables\Services\DataTable;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class CertificatesDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image', function($row) {
                return '<img src="'.asset($row->getFirstMedia()->getUrl('thumb')).'">';
            })
            ->editColumn('page_name', function($row) {
                if(is_null($row->shop_id)){
                    return '<span class="text text-danger">Silinib və ya mövcud deyil!</span>';
                }
                elseif(is_null($row->deleted_page)){
                    return $row->page_name;
                }
                else{
                    return '<span class="text text-danger" style="text-decoration: line-through">'.$row->page_name.'</span>';
                }
            })
            ->rawColumns(['action','image','page_name'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', [
                    'route' => 'certificates',
                    'row' => $row,
                    'forceDelete' => true,
                    'largeModal' => true,
                ])->render();
            });
    }

    public function query(Certificates $model)
    {
        $query = $model
            ->leftJoin('page_translations as pt', 'pt.id', '=', 'certificates.shop_id')
            ->newQuery()->with('media')
            ->select('certificates.*','pt.name as page_name','pt.deleted_at as deleted_page');

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'certificates.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Şəkil', 'orderable' => false, 'searchable' => false],
            ['data' => 'page_name', 'name' => 'pt.page_name', 'title' => 'Mağaza'],
            ['data' => 'title_az', 'name' => 'certificates.title_az', 'title' => 'Ad az'],
            ['data' => 'no', 'name' => 'certificates.no', 'title' => 'No'],
            ['data' => 'created_at', 'name' => 'certificates.created_at','title' => 'Yaradıldı', 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'certificates.updated_at', 'title' => 'Yenilənib', 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }



    protected function filename()
    {
        return 'certificatessdatatable_' . time();
    }
}
