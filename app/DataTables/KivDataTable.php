<?php

namespace App\DataTables;

use App\Models\Kiv;
use Yajra\DataTables\Services\DataTable;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class KivDataTable extends DataTable
{

    protected $route,$kivType;

    public function route($route,$kivType) {
        $this->route = $route;
        $this->kivType = $kivType;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image', function($row) {
                if($this->kivType == 1){
                    return '<img src="'.asset($row->getFirstMedia('logo_image')->getFullUrl()).'" style="max-width: 250px;max-height: 250px;">';
                }else{
                    return '<img src="'.asset($row->getFirstMedia()->getUrl('thumb')).'">';
                }
            })
            ->editColumn('page_name', function($row) {
                if(is_null($row->tid)){
                    return '<span class="text text-danger">Silinib və ya mövcud deyil!</span>';
                }
                elseif(is_null($row->deleted_page)){
                    return $row->page_name;
                }
                else{
                    return '<span class="text text-danger" style="text-decoration: line-through">'.$row->page_name.'</span>';
                }
            })
            ->rawColumns(['action','image','page_name'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', [
                    'route' => $this->route,
                    'row' => $row,
                    'forceDelete' => true,
                    'largeModal' => true,
                ])->render();
            });
    }

    public function query(Kiv $model)
    {
        $query = $model->newQuery()
            ->where('kivs.kiv_type',$this->kivType);
        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'kivs.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Şəkil', 'orderable' => false, 'searchable' => false],
            ['data' => 'created_at', 'name' => 'kivs.created_at','title' => 'Yaradıldı','searchable' => false],
            ['data' => 'updated_at', 'name' => 'kivs.updated_at', 'title' => 'Yenilənib','searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }



    protected function filename()
    {
        return 'kivLogosdatatable_' . time();
    }
}
