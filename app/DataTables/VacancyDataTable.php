<?php

namespace App\DataTables;

use App\Models\Vacancy;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class VacancyDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->rawColumns(['action','work_information'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'vacancies','forceDelete' =>true,'translation' =>true,'largeModal' => true])->render();
            });
    }

    public function query(Vacancy $model)
    {
        $query = $model->newQuery()
            ->join('vacancy_translations as vt', 'vt.vacancy_id', '=', 'vacancies.id')
            ->select(
                'vt.*',
                'vt.id as tid',
                'vacancies.id',
                'vacancies.published_by',
                'vacancies.end_date'
            );

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('vt.lang', $this->request()->get('lang'));
        }

        if($this->request()->get('type') == 1){
            $query->where('vacancies.end_date', '>=', date('Y-m-d'));
        }
        elseif($this->request()->get('type') == 2){
            $query->where('vacancies.end_date', '<', date('Y-m-d'));
        }
        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '140px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'vt.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'position', 'name' => 'vt.position', 'title' => 'Vəzifə','searchable' => true],
            ['data' => 'published_by', 'name' => 'vacancies.published_by', 'title' => 'Elanın başlanma tarixi', 'searchable' => false],
            ['data' => 'end_date', 'name' => 'vacancies.published_by', 'title' => 'Elanın bitmə tarixi', 'searchable' => false],
            ['data' => 'lang', 'name' => 'vt.lang', 'title' => 'Dil', 'searchable' => false,'orderable' => false],
            ['data' => 'work_information', 'name' => 'vt.work_information', 'title' => 'İş barədə məlumat','orderable' => false, 'searchable' => false,'class' => 'none'],
            ['data' => 'created_at', 'name' => 'vt.created_at', 'title' => 'Yaradıldı','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'vt.updated_at', 'title' => 'Yenilənib','orderable' => false, 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [1,'desc'] ],
            'lengthMenu' => [10,25,50],
            'language' => [
                'url' => url('lang.json'),
            ],
            'drawCallback' => 'function() {
                $("#articles_length").prependTo($("#dataTables_length_box"));
            }',
        ];
    }
}
