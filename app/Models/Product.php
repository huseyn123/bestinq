<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Auth;
use Db;

class Product extends Model implements HasMedia
{
    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    public static $type ='image';
    public static $width =640;
    public static $height =426;

    public static $gallery_width =720;
    public static $gallery_height =530;

    public static $cover_width =1920;
    public static $cover_height =300;


    public static function rules($id){

        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'name' => "required",
            'slug' => 'unique:product_translations,slug,'.$id,
            'image' => $img.'|max:10000',
            'cover' => 'dimensions:min_width='.self::$cover_width.',min_height='.self::$cover_height.'|max:10000',
            'page_id' => 'required|exists:page_translations,id',
            'meta_description' => 'nullable|max:500',
        ];
    }

    public static function parameterRules()
    {
        return [
            'image' => 'max:10000',
            'cover' => 'dimensions:min_width='.self::$cover_width.',min_height='.self::$cover_height.'|max:10000',
        ];
    }

    public static $messages = [
        'image.required' => "Şəkil əlavə olunmayıb",
        'page_id.required' => 'Kateqoriya seçilməyib',
        'cover.dimensions' => 'Kover şəkilin ölçüsü :min_width x :min_height nisbətdə olmalıdır.',
    ];

    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }



    public function relatedPages()
    {
        return $this->hasMany('App\Models\ProductTranslation', 'product_id', 'id');
    }

    public function trans()
    {
        return $this->hasOne(ProductTranslation::class, 'product_id')->orderBy('product_translations.id', 'asc');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit('stretch',145,145)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('thumb')
            ->fit('stretch',300,100)
            ->performOnCollections('cover')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$cover_width,self::$cover_height)
            ->performOnCollections('cover')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('thumb')
            ->fit('stretch',320,130)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$gallery_width,self::$gallery_height)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();


    }

}

