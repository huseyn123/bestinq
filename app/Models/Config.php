<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'settings';

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static $rules = [
        'value' => 'nullable'
    ];

    public static $messages = [
        //'required' => 'Bütün xanaların doldurulması zəruridir!',
    ];
}
