<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Certificates extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];

    public static function rules($id)
    {
        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'title_az'    => 'required',
            'image' => $img.'|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=360,min_height=470',
            'no'    => 'required',
            'shop_id' => 'required|not_in:0',
        ];
    }

    public static $messages = [
        'title_az.required' => 'Ad az doldurulmayıb.',
        'image.required' => 'Şəkil seçilməyib',
        'no.required' => 'Nömre doldurulmayıb.',
        'shop_id.required' => 'Mağaza seçilməyib.',
        'shop_id.not_in' => 'Mağaza seçilməyib.',
    ];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function registerMediaConversions(Media $media = null)
    {

        $this->addMediaConversion('thumb')
            ->fit('stretch',270,350)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',360,470)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();


    }
}
