<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Vacancy extends Model
{
    use SoftDeletes;

    protected $table='vacancies';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates =   ['created_at', 'updated_at', 'deleted_at'];



    public static function rules($required = 'required|')
    {
        return [
//            'name' => 'nullable',
//            'image' => $required . '|mimes:jpeg,jpg,png|max:10000',
//            'site_url' => 'nullable',
        ];
    }

    public static $parameterRules = [

    ];


    public static $messages = [
        'file.required' => "Loqotip əlavə olunmayıb."
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function relatedVacancies()
    {
        return $this->hasMany('App\Models\VacancyTranslation', 'vacancy_id');
    }


    public function trans()
    {
        return $this->hasOne(VacancyTranslation::class, 'vacancy_id');
    }
}

