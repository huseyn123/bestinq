<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleTranslation extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];

    public static function rules($id){
        return [
            'name' => 'required|max:255',
            'page_id' => 'required',
            'slug' => 'unique:article_translations,slug,'.$id.',id',
            'summary' => 'nullable',
            'content' => 'nullable',
        ];
    }

    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb.",
        'page_id.required' => 'Kateqoriya seçilməyib',
    ];


    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }

}
