<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;


class Slider extends Model implements HasMedia
{
    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static $width =1920;
    public static $height =500;

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static function rules($required,$parent_id = null){
        self::$width = 1920;
        self::$height = 450; ;
        return [
            'title' => 'nullable',
            'summary' => 'nullable',
            'image' => $required.'dimensions:min_width='.self::$width.',min_height='.self::$height,
            'm_image' => $required.'dimensions:min_width=775,min_height=450',
        ];
    }

    public static $messages = [
        'image.required' => "Şəkil əlavə edilməyib.",
        'm_image.required' => "Mobil Şəkil əlavə edilməyib.",
        'm_image.dimensions' => 'Mobil Şəkilin ölçüsü :min_width x :min_height nisbətdə olmalıdır.',
    ];


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(300)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();   //for datatable

        $this->addMediaConversion('view')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();//for blade

        $this->addMediaConversion('thumb')
            ->width(300)
            ->performOnCollections('mobile')
            ->keepOriginalImageFormat();   //for datatable

        $this->addMediaConversion('view')
            ->fit('stretch',775,450)
            ->performOnCollections('mobile')
            ->keepOriginalImageFormat();//for blade

    }
}
