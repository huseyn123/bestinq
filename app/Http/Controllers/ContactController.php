<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Config;
use App\Models\Dictionary;
use App\Mail\ContactForm;
use Mail;
use App\Mail\ApplicationForm;

class ContactController extends Controller
{

    private $lang;

    public function __construct()
    {
        $this->lang = app()->getLocale();
    }

    public function apply_cv(Request $request)
    {
        $email = Config::where('key', 'contact_email')->firstOrFail();

        $inputs = [
            'full_name' => 'required',
            'image' => 'required|mimes:png,jpeg,jpg,pdf,doc,docx,zip,rar,zim|max:10000',
            'email' => 'required|email',
        ];

        $validation = Validator::make($request->all(), $inputs);

        if($validation->fails()){
            $this->responseDataTable(1, $validation->errors()->first());
        }

        try {

            Mail::to($email->value)->send(new ContactForm($request->all(), 'contact', 'Onlayn Müraciət',[$request->file('image')]));

            $response = $this->responseDataTable(0,'Müraciətiniz tamamlandi');
            return $this->responseJson($response);

        } catch (\Exception $e) {
            $response = $this->responseDataTable(1, $e->getMessage());
            return $this->responseJson($response);
        }

    }

    private function sendMail($pdf, $form,$files, $email)
    {
        Mail::to($email)->send(new ApplicationForm($pdf, $form, array_filter($files),'Onlayn Müraciət'));

    
      try {
            $response = $this->responseDataTable(0, $dictionary->content);
            return $this->responseJson($response);

        } catch (\Exception $e) {
            $response = $this->responseDataTable(1, $e->getMessage());
            return $this->responseJson($response);
        }

    }

}
