<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Logic\Menu;
use App\Logic\Share;
use App\Logic\WebCache;
use App\Models\Article;
use App\Models\ArticleTranslation;
use App\Models\Config;
use App\Models\Page;
use App\Logic\Pages;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Vacancy;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Http\Request;
use App\Models\Dictionary;
use Mail;
use App\Mail\ContactForm;


class PageController extends Controller
{
    use Share, Pages;

    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('ajax')->only(['postContact']);

        $this->loadData();
    }

    public function index()
    {
        
        $articles = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->where('articles.status', 1)
            ->where('articles.show_index', 1)
            ->where('pages.template_id',3)
            ->where('at.lang', $this->lang)
            ->whereNull('pt.deleted_at')
            ->whereNull('at.deleted_at')
            ->orderBy('articles.published_at', 'desc')
            ->with('media')
            ->select('articles.id','at.article_id','at.name',
                'at.slug', 'at.summary', 'articles.published_at',
                'pt.name as page_name', 'pt.slug as page_slug')
            ->limit(3)
            ->get();

        $blog_news = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->where('articles.status', 1)
            ->where('articles.show_index', 1)
            ->where('pages.template_id',22)
            ->where('at.lang', $this->lang)
            ->whereNull('pt.deleted_at')
            ->whereNull('at.deleted_at')
            ->orderBy('articles.published_at', 'desc')
            ->with('media')
            ->select('articles.id','at.article_id','at.name',
                'at.slug', 'at.summary', 'articles.published_at',
                'pt.name as page_name', 'pt.slug as page_slug')
            ->limit(3)
            ->get();

        $product_page = Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
            ->where('pt.lang', $this->lang)
            ->whereNull('pt.deleted_at')
            ->where('template_id',21)
            ->whereNotIn('pages.visible', [0])
            ->orderBy('pt.order', 'asc')
            ->select('pages.id','pages.template_id','pt.id as tid','pt.slug','pt.name')
            ->get();

        $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->where('products.featured',1)
            ->whereIn('pages.template_id',[21])
            ->select('ptr.*','pt.slug as page_slug','products.id')
            ->with('media')
            ->get();

        $relatedPages = [];

        foreach(config("app.locales") as $key => $locale){
            $relatedPages[$key]='/'.($key == $this->lang ? '' : $key);

        }
        return view('web.index', ['articles' => $articles,'relatedPages' => $relatedPages,'product_page' =>$product_page,'products' =>$products,'blog_news' =>$blog_news]);
    }

    public function showPage($slug1, $slug2 = null, $slug3 = null)
    {
        $slugs = array_filter([$slug1, $slug2, $slug3]);

        $checkPages = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->whereIn('pt.slug', $slugs)
            ->where('pt.lang', $this->lang)
            ->whereNull('pt.deleted_at')
            ->orderByRaw('FIELD(pt.slug, "'.end($slugs).'")')
            ->select('pages.template_id', 'pages.visible', 'pt.*', 'pt.id as tid', 'pages.id')
            ->get();


        if(count($slugs) != $checkPages->count()){
            if((!is_null($slug2) && $checkPages->count() > 0) && ( in_array($checkPages->first()->template_id,[3,22]) || in_array($checkPages->last()->template_id,[3,22] ))){   //maybe it is an article
                return $this->articleSingle($checkPages, end($slugs));
            }
            elseif((!is_null($slug2) && $checkPages->count() > 0) && ( in_array($checkPages->first()->template_id,[21]) || in_array($checkPages->last()->template_id,[21] ))){   //maybe it is an article
                return $this->productSingle($checkPages, end($slugs));
            }
            else{
                return view('errors.404');
            }
        }


        $getPage = $checkPages->last();

        $relatedPages = $this->menu->relatedPages($getPage);
//        $canonical_url = $this->menu->fullNestedSlug($getPage->parent_id, $getPage->slug,$this->lang);
        $canonical_url = url($getPage->slug);
        if (!is_null($getPage->forward_url)) {
            return redirect()->away($getPage->forward_url);
        }

        if ($getPage->template_id == 1) {
            return $this->dropdown($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 3) {
            return $this->news($getPage, $relatedPages,$canonical_url);
        }
        elseif ($getPage->template_id == 4) {
            return $this->static($getPage, $relatedPages,'contact',$canonical_url);
        }
        elseif ($getPage->template_id == 6) {
            return $this->static_block($getPage, $relatedPages,'about',$canonical_url);
        }
        elseif ($getPage->template_id == 7) {
            return $this->static_block($getPage, $relatedPages,'quality',$canonical_url);
        }
        elseif ($getPage->template_id == 8) {
            return $this->static_block($getPage, $relatedPages,'product_return',$canonical_url);
        }
        elseif ($getPage->template_id == 9) {
            return $this->static($getPage, $relatedPages,'satisfaction',$canonical_url);
        }
        elseif ($getPage->template_id == 10) {
            return $this->static_block($getPage, $relatedPages,'privacy',$canonical_url);
        }
        elseif ($getPage->template_id == 11) {
            return $this->static_block($getPage, $relatedPages,'use_website',$canonical_url);
        }
        elseif ($getPage->template_id == 12) {
            return $this->static_block($getPage, $relatedPages,'customer_rights',$canonical_url);
        }
        elseif ($getPage->template_id == 13) {
            return $this->static($getPage, $relatedPages,'company_manager',$canonical_url);
        }
        elseif ($getPage->template_id == 14) {
            return $this->static($getPage, $relatedPages,'mission',$canonical_url);
        }
        elseif ($getPage->template_id == 15) {
            return $this->vacancies($getPage, $relatedPages,$canonical_url);
        }
        elseif ($getPage->template_id == 16) {
            return $this->static_block($getPage, $relatedPages,'wholesale',$canonical_url);
        }
        elseif ($getPage->template_id == 17) {
            return $this->branch($getPage, $relatedPages,$canonical_url);
        }
        elseif ($getPage->template_id == 18) {
            return $this->faq($getPage, $relatedPages,$canonical_url);
        }
        elseif ($getPage->template_id == 19) {
            return $this->certificates($getPage, $relatedPages,$canonical_url);
        }
        elseif ($getPage->template_id == 20) {
            return $this->kiv($getPage, $relatedPages,$canonical_url);
        }
        elseif ($getPage->template_id == 21) {
            return $this->products($getPage, $relatedPages,$canonical_url);
        }
        elseif ($getPage->template_id == 22) {
            return $this->news($getPage, $relatedPages,$canonical_url);
        }
        else{
            return view('errors.404');
        }
    }

    public function applyResume($slug, $id){

        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')->where('pt.slug', $slug)->first();

        $relatedPages =  $this->menu->relatedPages($page);
        $vacancy = Vacancy::join('vacancy_translations as vt','vt.vacancy_id','vacancies.id')
            ->where('vt.lang',$this->lang)
            ->where('vacancies.id',$id)
            ->select('vt.*','vacancies.published_by as start_date','vt.id as tid')
            ->first();

        return view("web.vacancy_apply", ['page' => $page,'vacancy' => $vacancy,'relatedPages' => $relatedPages]);
    }

    public function postContact(Request $request)
    {
        $inputs = [
            'full_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ];


        $message = $this->dictionary['post_contact_subject'];

        return $this->send_mail($request,$inputs,$message);
    }

    public function send_mail($form,$validate_input,$message){
        $dictionary = Dictionary::where('keyword', 'email_sent')->where('lang_id', $this->lang)->first();
        $email = Config::where('key', 'contact_email')->firstOrFail();

        $validation = Validator::make($form->all(), $validate_input);

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
            return $this->responseJson($response);
        }

        try {
            Mail::to($email->value)->send(new ContactForm($form->all(),'contact',$message));
            $response = $this->responseDataTable(0, $dictionary->content);
            return $this->responseJson($response);

        } catch (\Exception $e) {
            $response = $this->responseDataTable(1, $e->getMessage());
            return $this->responseJson($response);
        }

    }

}