<?php

namespace App\Http\Controllers;
use App\Models\ProductTranslation;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Logic\Slug;
use DB;

class ProductTranslationController extends Controller
{
    private $order, $requests;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['order']);

        $this->requests = $request->except('_token', '_method');
    }


    public function store(Request $request)
    {
        $response = $this->validation($request->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $request->lang );


       $product = ProductTranslation::create($this->requests);

        return $this->responseJson($response);
    }


    public function update(Request $request, $id)
    {
        $product = ProductTranslation::findOrFail($id);

        $response = $this->validation($product->lang, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $request->lang );


        foreach($this->requests as $key => $put){
            $product->$key = $put;
        }
        $product->save();

        return $this->responseJson($response);
    }


    public function trash($id)
    {
        $product = ProductTranslation::findOrFail($id);
        $product->delete();

        $response = $this->responseDataTable(0,"", "#products", "#modal-confirm");
        return $this->responseJson($response);
    }

    public function restore($id)
    {
        $producr = ProductTranslation::onlyTrashed()->findOrFail($id);
        $producr->restore();

        $response = $this->responseDataTable(0,"", "#products", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $product =  ProductTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = ProductTranslation::where('product_id', $product->product_id)->withTrashed()->count();

        if($relatedPages == 1){
            Product::where('id', $product->product_id)->forceDelete();
        }
        else{
            $product->forceDelete();
        }

        $response = $this->responseDataTable(0,"", "#products", "#modal-confirm");
        return $this->responseJson($response);
    }

    private function validation($lang, $id = null)
    {
        $validation = Validator::make(request()->all(), ProductTranslation::rules($id), ProductTranslation::$messages);

        $response = $this->responseDataTable(0,"", "#products", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
