<?php

namespace App\Http\Controllers;

use App\Crud\KivLogoCrud;
use App\Logic\Kivs;
use Illuminate\Http\Request;
use App\Models\Kiv;
use App\DataTables\KivDataTable;
use Illuminate\Support\Facades\Validator;
use DB;

class KivLogoController extends Controller
{
    use Kivs;
    private $crud,$requests,$title,$route,$createName,$model,$view,$create_title,$kivType,$lang_tab;

    public function __construct(Request $request,KivLogoCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->view = 'kivs';
        $this->kivType = 1;
        $this->create_title = 'Yeni Loqo';
        $this->route = 'kivLogo';
        $this->title = "Şirkətin loqosu";
        $this->createName = "Yeni loqo";
        $this->model = 'App\Models\Kiv';
        $this->crud = $crud;


        $this->requests = $request->except('_token', '_method','image','png_file','eps_file','logo_image');
    }

}
