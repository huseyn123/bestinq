<?php

namespace App\Http\Controllers;

use App\Crud\BranchCrud;
use App\DataTables\BranchDataTable;
use App\Logic\Simple;
use Illuminate\Http\Request;
use App\Models\Branch;
use Illuminate\Support\Facades\Validator;
use DB;

class BranchController extends Controller
{
    use Simple;

    private $crud,$requests,$title,$route,$createName,$model,$view,$create_title,$lang_tab;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);
        $this->requests = $request->except('_token', '_method');


        $this->view = 'branch';
        $this->create_title = 'Yeni Əhatə dairəsi';
        $this->route = 'branch';
        $this->title = "Əhatə dairəsi";
        $this->createName = "Yeni Əhatə dairəsi";
        $this->model = 'App\Models\Branch';
        $this->crud = new BranchCrud();


    }


    public function index(BranchDataTable $dataTable)
    {
        return $dataTable->route($this->route)->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }




}
