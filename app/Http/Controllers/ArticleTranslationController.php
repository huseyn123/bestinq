<?php

namespace App\Http\Controllers;
use App\Logic\Order;
use App\Models\Article;
use App\Models\ArticleTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Logic\Slug;
use DB;

class ArticleTranslationController extends Controller
{
    private $requests;

    public function __construct(Request $request, Order $order)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax');
        $this->requests = $request->except('_token', '_method');

    }


    public function store(Request $request)
    {
        $response = $this->validation();

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'articleTranslation', $request->lang );

        DB::beginTransaction();

        ArticleTranslation::create($this->requests);

        DB::commit();

        return $this->responseJson($response);
    }


    public function update(Request $request, $id)
    {
        $page = ArticleTranslation::findOrFail($id);

        $response = $this->validation($id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'articleTranslation', $request->lang );

        DB::beginTransaction();


        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }

        $page->save();

        DB::commit();

        return $this->responseJson($response);
    }


    public function trash($id)
    {
        $article = ArticleTranslation::findOrFail($id);
        $article->delete();

        $response = $this->responseDataTable(0,"", "#articles", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function restore($id)
    {
        $article = ArticleTranslation::onlyTrashed()->findOrFail($id);
        $article->restore();

        $response = $this->responseDataTable(0,"", "#articles", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $article = ArticleTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = ArticleTranslation::where('article_id', $article->article_id)->withTrashed()->count();

        if($relatedPages == 1){
            Article::where('id', $article->article_id)->forceDelete();
        }
        else{
            $article->forceDelete();
        }

        $response = $this->responseDataTable(0,"", "#articles", "#modal-confirm");
        return $this->responseJson($response);
    }


    private function validation($id = null)
    {
        $validation = Validator::make($this->requests, ArticleTranslation::rules($id), ArticleTranslation::$messages);

        $response = $this->responseDataTable(0,"", "#articles", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
