<?php

namespace App\Http\Controllers;

use App\Crud\KivVideoCrud;
use App\Logic\Kivs;
use Illuminate\Http\Request;
use App\Models\Kiv;
use App\DataTables\KivDataTable;
use Illuminate\Support\Facades\Validator;
use DB;

class KivVideoController extends Controller
{
    use Kivs;
    private $crud,$requests,$title,$route,$createName,$model,$view,$create_title,$kivType,$lang_tab;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->view = 'kivs';
        $this->kivType = 2;
        $this->create_title = 'Yeni Video';
        $this->route = 'kivVideo';
        $this->title = "Reklam çarxları";
        $this->createName = "Yeni Video";
        $this->model = 'App\Models\Kiv';
        $this->lang_tab = 'title';
        $this->crud = new KivVideoCrud();

        $this->requests = $request->except('_token', '_method','image');
    }
}
