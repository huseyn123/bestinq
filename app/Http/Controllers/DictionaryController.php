<?php

namespace App\Http\Controllers;

use App\Crud\DictionaryCrud;
use Illuminate\Http\Request;
use App\Models\Dictionary;
use App\DataTables\DictionaryDataTable;
use DB;

class DictionaryController extends Controller
{
    private $crud, $requests;
    public $title;

    public function __construct(DictionaryCrud $crud, Request $request)
    {
        $this->title = "Lüğət";
        $this->crud = $crud;
        $this->requests = $request->except('_token', '_method');

        if (in_array(strtolower($request->method()), ['put', 'patch', 'post'])) {
            clearCache('dictionary');
        }
    }


    public function index(DictionaryDataTable $dataTable)
    {
        return $dataTable->render('admin.dictionary', ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.dictionary.create', ['fields' => $fields, 'title' => $this->title]);
    }


    public function edit($id)
    {
        $word = Dictionary::findOrFail($id);
        $words = Dictionary::where('keyword', $word->keyword)->pluck('content', 'lang_id');

        return view('admin.dictionary.edit', ['title' => $word->keyword, 'info' => $word, 'words' => $words, 'route' => ['dictionary.update', $word->id] ]);
    }


    public function store(Request $request)
    {
        $inputs = $request->input('content');
        $keyword = $request->input('keyword');
        $editor = $request->input('editor', 0);

        $request->validate(Dictionary::$rules, Dictionary::$messages);

        DB::beginTransaction();

        foreach($inputs as $lang => $input){

            try{
                Dictionary::create(['keyword' => $keyword, 'content' => $input, 'lang_id' => $lang, 'editor' => $editor]);
            }
            catch(\Exception $e){
                DB::rollback();

                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
            }

        }

        DB::commit();

        return redirect()->route('dictionary.index');
    }


    public function update(Request $request, $id)
    {
        $word = Dictionary::findOrFail($id);

        $inputs = $request->input('content');

        $request->validate(['content' => 'nullable']);

        $find = Dictionary::where('keyword', $word->keyword)->pluck('lang_id')->toArray();


        DB::beginTransaction();


        foreach($inputs as $lang => $input){

            if(in_array($lang, $find))
            {
                try{
                    Dictionary::where('keyword', $word->keyword)->where('lang_id', $lang)->update(['content' => $input]);
                }
                catch(\Exception $e){
                    DB::rollback();

                    $response = $this->responseDataTable(1, $e->getMessage());
                    return $this->responseJson($response);
                }
            }
            else{
                try{
                    Dictionary::create(['keyword' => $word->keyword, 'content' => $input, 'lang_id' => $lang]);
                }
                catch(\Exception $e){
                    DB::rollback();

                    $response = $this->responseDataTable(1, $e->getMessage());
                    return $this->responseJson($response);
                }
            }

        }

        DB::commit();

        $response = $this->responseDataTable(0, trans('locale.update_success'),"#dictionary", "#myModal");
        return $this->responseJson($response);
    }
}
