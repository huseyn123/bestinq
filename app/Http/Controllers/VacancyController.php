<?php

namespace App\Http\Controllers;
use App\Crud\VacancyParameterCrud;
use App\Crud\VacancyCrud;
use App\Models\VacancyTranslation;
use App\Models\Vacancy;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\DataTables\VacancyDataTable;
use DB;

class VacancyController extends Controller
{
    private $crud, $requests, $title;

    public function __construct(VacancyCrud $crud,Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->requests = $request->except('_token', '_method');
        $this->crud = $crud;
        $this->title = "Vakansiyalar";
    }


    public function index(VacancyDataTable $dataTable)
    {
        return $dataTable->render('admin.vacancies', ['title' => $this->title]);
    }

    public function create()
    {
        $fields = $this->crud->fields(null, 'create');

        return view('admin.dt.create', ['title' => 'Yeni Vakansiya', 'fields' => $fields, 'route' => 'vacancies.store','editor'=>true,'translation' => true]);
    }


    public function store(Request $request)
    {
        if(count(config('app.locales')) == 1){
            $lang = app()->getLocale();
        }
        else{
            $lang = $request->lang;
        }

        $translates = ['vacancy_id','position','work_information','requirements','work_experience','salary','lang'];

        // check Validation
        $validation = Validator::make($this->requests, Vacancy::rules($lang, null), Vacancy::$messages);
        $response = $this->validation($validation, $lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }



        //inputs for models
        $vacancyInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $vacancyInputs);

        //store
        DB::beginTransaction();


        try{
            $vacancy = Vacancy::create($vacancyInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            $translationInputs['vacancy_id'] = $vacancy->id;
            VacancyTranslation::create($translationInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        DB::commit();

        return $this->responseJson($response);
    }


    public function edit($id)
    {
        $parameterCrud = new VacancyParameterCrud();
        $fields = [];
        $langs = [];
        $keys = [];



        $vacancies = Vacancy::join('vacancy_translations as vt', 'vt.vacancy_id', '=', 'vacancies.id')
            ->where('vt.id', $id)
            ->select(
                'vt.*',
                'vt.id as tid',
                'vacancies.id',
                'vacancies.published_by',
                'vacancies.end_date'

            )
            ->firstOrFail();


        if(count(config('app.locales')) == 1){
            return $this->singleEdit($vacancies);
        }

        $relatedVacancy = $vacancies->relatedVacancies;
        $parameters = $parameterCrud->fields('edit', $vacancies);

        foreach ($relatedVacancy as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);
        }

        return view('admin.page.edit-withlocale', ['info' => $vacancies, 'langs' => $langs, 'parameters' => $parameters, 'pageId' => $vacancies->vacany_id, 'fields' => $fields, 'relatedPage' => $relatedVacancy,'model' => 'vacancies', 'route' => 'vacancyTranslation','editor2' =>true]);
    }


    private function singleEdit($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);

        $keys = array_filter(explode(",", $page->meta_keywords));

        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'keys' => $keys, 'model' => 'page']);
    }


    public function update(Request $request, $id)
    {
        $vacancy = Vacancy::findOrFail($id);

        $validation = Validator::make($this->requests, Vacancy::$parameterRules, Vacancy::$messages);
        $response = $this->validation($validation, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        foreach($this->requests as $key => $put){
            $vacancy->$key = $put;
        }

        $vacancy->save();

        return $this->responseJson($response);
    }


    public function updateSingle(Request $request, $id)
    {
        $vacacyTranslation = VacancyTranslation::findOrFail($id);

        $translates = ['name', 'parent_id', 'lang', 'slug', 'order', 'content', 'forward_url', 'meta_description', 'meta_keywords'];

        // check Validation
        $validation = Validator::make($this->requests, Vacancy::rules($vacacyTranslation->lang, $id), Vacancy::$messages);
        $response = $this->validation($validation, $vacacyTranslation->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }


        //inputs for models
        $vacancyInput = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $vacancyInput);

        //store
        DB::beginTransaction();


        try{
            Vacancy::where('id', $vacacyTranslation->vacancy_id)->update($vacancyInput);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            foreach($translationInputs as $key => $put){
                $vacancyInput->$key = $put;
            }
            $vacacyTranslation->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        DB::commit();

        return $this->responseJson($response);
    }




    private function validation($validation, $lang, $id = null)
    {
        $response = $this->responseDataTable(0,"", "#vacancies", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function resize($templateId)
    {
        if($templateId == 11){
            $resizeImage = ['resize' => ['fit' => false, 'size' => [720, null]], 'thumb' => null ];
        }
        else{
            $resizeImage = ['resize' => ['fit' => false, 'size' => [1920, null]], 'thumb' => null ];
        }

        return $resizeImage;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
