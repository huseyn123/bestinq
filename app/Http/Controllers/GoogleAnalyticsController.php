<?php

namespace App\Http\Controllers;

use App\Models\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GoogleAnalyticsController extends Controller
{
    public function index()
    {
        return view('admin.analytic', ['title' => 'Google Analytics']);
    }


    public function store(Request $request)
    {
        $inputs = [
          'view_id' => 'nullable',
          'file' => 'nullable|mimetypes:application/json,text/plain|max:10000'
        ];

        $request->validate($inputs);

        $viewId = Config::where('key', 'google_analytic_view_id')->firstOrFail();

        $viewId->value = $request->view_id;
        $viewId->save();

        if($request->hasFile('file')){
            $request->file('file')->storeAs(
                'analytics', 'service-account-credentials.json'
            );
        }


        clearCache('config', false);

        return redirect()->back()->with('success', 'Əməliyyat uğurlu alındı');
    }
}
