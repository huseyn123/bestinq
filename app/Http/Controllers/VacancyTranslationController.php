<?php

namespace App\Http\Controllers;
use App\Models\Vacancy;
use App\Models\VacancyTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;

class VacancyTranslationController extends Controller
{
    private $requests;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax');
        $this->requests = $request->except('_token', '_method');

    }


    public function store(Request $request)
    {
        $response = $this->validation();

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        DB::beginTransaction();

        $vacancy = VacancyTranslation::create($this->requests);

        DB::commit();

        return $this->responseJson($response);
    }


    public function update(Request $request, $id)
    {
        $vacancy = VacancyTranslation::findOrFail($id);

        $response = $this->validation($id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        DB::beginTransaction();


        foreach($this->requests as $key => $put){
            $vacancy->$key = $put;
        }

        $vacancy->save();


        DB::commit();

        return $this->responseJson($response);
    }


    public function trash($id)
    {
        $vacancy = VacancyTranslation::findOrFail($id);
        $vacancy->delete();

        $response = $this->responseDataTable(0,"", "#vacancy", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function restore($id)
    {
        $vacancy = VacancyTranslation::onlyTrashed()->findOrFail($id);
        $vacancy->restore();

        $response = $this->responseDataTable(0,"", "#vacancy", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $vacancy = VacancyTranslation::findOrFail($id);

        $relatedVacancies = VacancyTranslation::where('vacancy_id', $vacancy->vacancy_id)->withTrashed()->count();

        if($relatedVacancies == 1){
            Vacancy::where('id', $vacancy->vacancy_id)->forceDelete();
        }
        else{
            $vacancy->forceDelete();
        }

        $response = $this->responseDataTable(0,"", "#vacancies", "#modal-confirm");
        return $this->responseJson($response);
    }


    private function validation($id = null)
    {
        $validation = Validator::make($this->requests, VacancyTranslation::rules(), VacancyTranslation::$messages);

        $response = $this->responseDataTable(0,"", "#vacancies", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
