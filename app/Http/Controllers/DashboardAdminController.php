<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;
use Analytics;
class DashboardAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!is_null(getConfig()['google_analytic_view_id'])){
            $fetchTopBrowsers = Analytics::fetchTopBrowsers(Period::months(1))->toArray();
            $fetchTopVisitors = Analytics::fetchTotalVisitorsAndPageViews(Period::create(Carbon::today()->subMonths(6)->startOfMonth(), Carbon::today()))->toArray();
            $fetchMostVisitedPages = Analytics::fetchMostVisitedPages(Period::months(6), 15)->toArray();

            $browsersColors = ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de', '#39cccc', '#f56954', '#f56954', '#f56954'];
            $browsers = [];
            $visitors = [];
            $month = range(1,12);
            $visitedMonths = [];
            $totalVisitors = 0;

            foreach ($fetchTopBrowsers as $key => $b){
                $browsers[$key]['value'] = $b['sessions'];
                $browsers[$key]['color'] = $browsersColors[$key];
                $browsers[$key]['highlight'] = $browsersColors[$key];
                $browsers[$key]['label'] = $b['browser'];
            }


            foreach ($fetchTopVisitors as $visitor) {
                $date = with(new Carbon($visitor['date']));

                if(in_array($date->month, $month)){

                    if($date->day ==1){
                        $totalVisitors = 0;
                    }

                    $totalVisitors = $totalVisitors + $visitor['visitors'];

                    $visitors[$date->month] = $totalVisitors;
                }
            }


            array_walk($visitors, function($item, $key) use(&$visitedMonths) {
                $visitedMonths[] = trans('locale.config.months')[$key];
            });

            //dd($fetchMostVisitedPages);
        }

        return view('admin.dashboard', ['title' => 'Dashboard', 'browsers' => $browsers ?? [], 'visitors' => array_values($visitors ?? []), 'visitedMonths' => $visitedMonths ?? [], 'mostVisited' => $fetchMostVisitedPages ?? []]);
    }
}
