<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    public function logout(Request $request)
    {
        if(Auth::guard('admin')->check() && basename(url()->previous()) == 'admin'){
            $path = '/admin/login';
            $guard = 'admin';
        }
        else{
            $path = '/';
            $guard = 'web';
        }

        Auth::guard($guard)->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect($path);
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout']);
    }


    protected function guard()
    {
        return Auth::guard('web');
    }


    public function showLoginForm()  //this will be deleted
    {
        return view('web.index');
    }
}
