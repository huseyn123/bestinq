<?php

namespace App\Http\Controllers;

use App\DataTables\CartDataTable;
use App\Logic\Share;
use Illuminate\Http\Request;
use Hash;

class UserController extends Controller
{
    use Share;

    public function __construct(Request $request)
    {
        $this->middleware('auth:web');
        $this->loadData();
    }


    public function show()
    {
        $user = auth()->guard('web')->user();

        return view('web.user.profile', ['user' => $user]);
    }



    public function update(Request $request)
    {
        $user = auth()->guard('web')->user();

        $inputs = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address
        ];

        $rules = [
            'name' => 'required|min:6',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'address' => 'nullable'
        ];

        $request->validate($rules);


        $user->where('id', $user->id)->update($inputs);

        return redirect()->back();
    }



    public function updatePassword(Request $request)
    {
        $user = auth()->user();

        $inputs = [
            'current_password' => 'required|current_password_match',
            'password'     => 'required|min:6|confirmed',
        ];

        $request->validate($inputs);

        $updated = $user->update([ 'password' => Hash::make($inputs['password']) ]);

        if ($updated) {
            request()->session()->flash('message', "Şifrəniz yeniləndi");
        }
        else{
            request()->session()->flash('error', 'Daxili xəta baş verdi');
        }

        return redirect()->back();
    }


    public function cart(CartDataTable $dataTable)
    {
        $user = auth()->user();

        return $dataTable->render('web.user.cart', ['user' => $user]);
    }
}
