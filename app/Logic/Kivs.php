<?php

namespace App\Logic;

use App\DataTables\KivDataTable;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait Kivs
{

    public function index(KivDataTable $dataTable)
    {
        return $dataTable->route($this->route,$this->kivType)->render('admin.'.$this->view.'.index', ['title' => $this->title, 'route' => $this->route]);

    }
    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.'.$this->view.'.create', ['route' => $this->route.'.store','title' => $this->title,'createName' => $this->createName,'create_title' => $this->create_title,'lang_tab' => $this->lang_tab,'fields' => $fields]);

    }

    public function store(Request $request)
    {
        $response = $this->validation();

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['kiv_type'] = $this->kivType;
        $data = $this->model::create($this->requests);

        if($request->hasFile('image')) {
            try{
                $data
                    ->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

         if($request->hasFile('logo_image')) {
             try{
                 $data
                     ->addMedia($request->logo_image)
                     ->toMediaCollection('logo_image');
             }
             catch(\Exception $e){
                 DB::rollback();
                 return $this->errorDt($e->getMessage());
             }
         }

        if($request->hasFile('png_file')) {
            try{
                $data
                    ->addMedia($request->png_file)
                    ->toMediaCollection('png_file');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if($request->hasFile('eps_file')) {
            try{
                $data
                    ->addMedia($request->eps_file)
                    ->toMediaCollection('eps_file');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }


        return $this->responseJson($response);
    }

    public function edit($id)
    {
        $data = $this->model::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.'.$this->view.'.edit', ['title' => 'Düzəliş et','data' => $data,'lang_tab' => $this->lang_tab,'fields'=>$fields,'route' => [$this->route.'.update', $id]]);
    }

    public function update(Request $request, $id)
    {
        $data = $this->model::findOrFail($id);

        $response = $this->validation($id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        if($request->hasFile('image')) {
            try{
                $data->clearMediaCollection();  //delete old file
                $data
                    ->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if($request->hasFile('logo_image')) {
            try{
                $data->clearMediaCollection('logo_image');  //delete old file
                $data
                    ->addMedia($request->logo_image)
                    ->toMediaCollection('logo_image');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if($request->hasFile('png_file')) {
            try{
                $data->clearMediaCollection('png_file');  //delete old file
                $data
                    ->addMedia($request->png_file)
                    ->toMediaCollection('png_file');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if($request->hasFile('eps_file')) {
            try{
                $data->clearMediaCollection('eps_file');  //delete old file
                $data
                    ->addMedia($request->eps_file)
                    ->toMediaCollection('eps_file');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseJson($response);
    }

    public function destroy($id)
    {
        $data =  $this->model::findOrFail($id);
        $data->forceDelete();

        $response = $this->responseDataTable(0,"", "#". $this->route, "#modal-confirm");
        return $this->responseJson($response);
    }

    private function validation($id = null)
    {
        $validation = Validator::make(request()->all(), $this->model::rules($id,$this->kivType), $this->model::$messages);

        $response = $this->responseDataTable(0,"", "#".$this->route, '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }

    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }

}
