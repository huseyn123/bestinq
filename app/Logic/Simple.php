<?php

namespace App\Logic;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait Simple
{


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.'.$this->view.'.create', ['route' => $this->route.'.store','title' => $this->title,'createName' => $this->createName,'create_title' => $this->create_title,'lang_tab' => $this->lang_tab,'fields' => $fields]);

    }

    public function store(Request $request)
    {
        $response = $this->validation();

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $data = $this->model::create($this->requests);

        return $this->responseJson($response);
    }

    public function edit($id)
    {
        $data = $this->model::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.'.$this->view.'.edit', ['title' => 'Düzəliş et','data' => $data,'lang_tab' => $this->lang_tab,'fields'=>$fields,'route' => [$this->route.'.update', $id]]);
    }

    public function update(Request $request, $id)
    {
        $data = $this->model::findOrFail($id);

        $response = $this->validation($id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseJson($response);
    }

    public function destroy($id)
    {
        $data =  $this->model::findOrFail($id);
        $data->forceDelete();

        $response = $this->responseDataTable(0,"", "#". $this->route, "#modal-confirm");
        return $this->responseJson($response);
    }

    private function validation($id = null)
    {
        $validation = Validator::make(request()->all(), $this->model::rules($id), $this->model::$messages);

        $response = $this->responseDataTable(0,"", "#".$this->route, '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }

    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }

}
