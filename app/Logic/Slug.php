<?php

namespace App\Logic;

use Cocur\Slugify\Slugify;

class Slug
{

    public static function model($table){

        $model = config('config.model');

        return $model[$table];
    }



    public static function slugify($slug, $title, $table, $lang, $trashed = true)
    {
        $slugify = new Slugify(['rulesets' => ['default', config('config.slug_replacement.'.$lang)]]);
        if(trim($slug) == '')
        {
            $generateSlug = $slugify->slugify($title);
            $uniqueSlug = self::uniqueSlug($generateSlug, $table, $trashed);
            return $uniqueSlug;
        }
        else{
            $generateSlug = $slugify->slugify($slug);
            return $generateSlug;
        }
    }



    public static function uniqueSlug($slug, $table, $trashed){

        $model = self::model($table);

        $num = -1;
        $default_slug = $slug;

        do{
            $num++;

            if($num > 0){
                $slug = $default_slug.'-'.$num;
            }

            if($trashed == true){
                $rand = $model::where('slug', $slug)->withTrashed()->first();
            }
            else{
                $rand = $model::where('slug', $slug)->first();
            }
        }

        while(!empty($rand));

        return $slug;
    }

}