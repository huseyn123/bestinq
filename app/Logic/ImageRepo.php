<?php

namespace App\Logic;

use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class ImageRepo
{
    public function store($file, $resize)
    {
        $manager = new ImageManager();

        $extension = $file->getClientOriginalExtension();
        $image = $manager->make($file);
        $imageSize = [$image->width(), $image->height()];
        $date = Carbon::today()->format('Y-m-d');
        $hash = md5(explode('.',$file->getClientOriginalName())[0].'-'.time());
        $fileName = $hash.'.'.$extension;
        $path = $date.'/'.$fileName;
        $thumbPath = 'thumb/'.$path;

        if(!is_null($resize)){

            if($imageSize > $resize['resize']['size']){
                if($resize['resize']['fit'] == true){
                    $image->fit($resize['resize']['size'][0], $resize['resize']['size'][1]);
                }
                else{
                    $image->resize($resize['resize']['size'][0], $resize['resize']['size'][1], function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
            }

            if(!is_null($resize['thumb'])){
                $this->thumbStore($file, $thumbPath, $resize);
            }
        }

        Storage::put("public/$path",  $image->stream());

        return $path;
    }


    private function thumbStore($file, $thumbPath, $resize)
    {
        $manager = new ImageManager();

        $imageThumb = $manager->make($file);

        if($resize['thumb']['fit'] == true){
            $imageThumb->fit($resize['thumb']['size'][0], $resize['thumb']['size'][1]);
        }
        else{
            $imageThumb->resize($resize['thumb']['size'][0], $resize['thumb']['size'][1], function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $imageThumb->encode($file->getClientOriginalExtension(), 90);

        Storage::put("public/$thumbPath",  $imageThumb->stream());
    }


    public function deleteFile($filename)
    {
        if(!is_null($filename)){
            Storage::delete(["public/$filename", "public/thumb/$filename"]);
        }
    }
}