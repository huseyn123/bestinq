<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;


class Breadcrumb extends Menu
{
    private function build($thisPage)
    {
        $bc = [];

        $pages = $this->all();

        $breadcrumb = $this->nestedBuild($pages, $thisPage);

        foreach ($breadcrumb as $data){
            $bc[] = $data['breadcrumb'];
        }

        return $bc;
    }


    public function get($thisPage)
    {
        $result = $this->build($thisPage);

        return $result;
    }
}