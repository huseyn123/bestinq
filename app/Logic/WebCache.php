<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\ApartmentElement;
use App\Models\Config;
use App\Models\Dictionary;
use App\Models\Slider;
use App\Models\Partner;
use App\Models\Page;
use DB;
use Cache;

class WebCache
{
    public function getDictionary($lang)
    {
        $collect = [];

        $dictionary = Cache::rememberForever("dictionary_$lang", function () use($collect, $lang)
        {
            return Dictionary::where('lang_id', $lang)->select('keyword', 'content')->orderBy('keyword', 'asc')->pluck('content', 'keyword')->toArray();
        });

        return $dictionary;
    }

    public function getSlider($lang)
    {
        $slider = Cache::rememberForever("slider_$lang", function () use($lang){
            return Slider::with('media')->orderBy('order', 'asc')->where('lang',$lang)->limit(10)->get();
        });

        return $slider;
    }

    public function getPartner()
    {
        $partners = Cache::rememberForever("partners", function () {
            return Partner::with('media')->orderBy('order', 'desc')->limit(40)->get();
        });

        return $partners;
    }

    public static function config()
    {
        $config = Cache::rememberForever('config', function () {
               return Config::all();
        });

        return $config;
    }
}