<?php

namespace App\Logic;
use App\Models\Article;
use App\Models\Banner;
use App\Models\Branch;
use App\Models\BranchElement;
use App\Models\Certificates;
use App\Models\Faq;
use App\Models\Kiv;
use App\Models\Page;
use App\Models\ArticleTranslation;
use App\Models\PageTranslation;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Vacancy;
use App\Models\VacancyTranslation;
use DB;
use function GuzzleHttp\Promise\queue;

trait Pages
{

    protected function productSingle($checkPages, $slug)
    {
        $category = $checkPages->first();
        $getProduct = Product::join('product_translations as pt', 'products.id', '=', 'pt.product_id')
            ->where('pt.slug', $slug)
            ->whereNull('pt.deleted_at')
            ->with('media')
            ->select('pt.*', 'pt.id as tid', 'products.id','products.template_id')
            ->firstOrFail();

        $relatedPages = $this->menu->relatedPages($checkPages->last(), $getProduct);

        $similarProducts = $this->similarProducts($getProduct->id,21,4,$category->tid)->get();
        $canonical_url = route('showPage',[$category->slug, $getProduct->slug]);

        return view('web.product-single',['product' => $getProduct,'similarProducts' => $similarProducts,'page' => $category,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function articleSingle($checkPages, $slug)
    {
        $category = $checkPages->first();
        $getArticle = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->where('at.slug', $slug)
            ->whereNull('at.deleted_at')
            ->with('media')
            ->select('at.*', 'at.id as tid', 'articles.*')
            ->firstOrFail();

        $relatedPages = $this->menu->relatedPages($checkPages->last(), $getArticle);
        $similarArticles = $this->similarArticles($getArticle->id,$category->template_id,6)->get();
        $canonical_url = route('showPage',[$category->slug, $getArticle->slug]);

        return view('web.article-single', ['article' => $getArticle,'similarArticles' => $similarArticles,'page' => $category,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function static($getPage, $relatedPages,$view,$canonical_url)
    {
        return view("web.".$view, ['page' => $getPage,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function static_block($getPage, $relatedPages,$view,$canonical_url,$template_id =[2])
    {
        $blocks = Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.parent_id',$getPage->tid)
            ->where('pt.lang',$this->lang)
            ->whereIn('pages.template_id',$template_id)
            ->select('pt.name','pt.content','pages.id')
            ->with('media')
            ->get();

        return view("web.".$view, ['page' => $getPage,'blocks' =>$blocks,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function branch($getPage, $relatedPages,$canonical_url)
    {
        $branches = Branch::join('branch_elements as region', function($data)
            {
                $data->on('region.id', '=', 'branches.region_id')
                    ->where('region.type', '=', "region");
            })
            ->join('branch_elements as network', function($data)
            {
                $data->on('network.id', '=', 'branches.network_id')
                    ->where('network.type', '=', "network");
            })
        ->select('branches.location_az as location','branches.phone','region.title_az as region','network.title_az as network')->paginate(12);

        $regions =BranchElement::where('type','region')->pluck('title_'.$this->lang.' as title','id');
        $networks =BranchElement::where('type','network')->pluck('title_'.$this->lang.' as title','id');

        return view("web.branch", ['page' => $getPage,'branches' =>$branches,'relatedPages' => $relatedPages,'regions' =>$regions,'networks' =>$networks,'canonical_url' => $canonical_url]);
    }

    protected function kiv($getPage, $relatedPages,$canonical_url)
    {
        if(request()->type && array_key_exists(request()->type,config('config.kiv_types'))){
            $type =request()->type;
        }else{
            $type =key(config('config.kiv_types'));
        }

        $canonical_url=$canonical_url.'?type='.$type;


        $kivs = Kiv::where('kiv_type',config('config.kiv_type_database.'.$type))->select('id','kiv_type','title_'.$this->lang.' as title','youtube_id')
            ->with('media')
            ->get();

        return view("web.kiv", ['page' => $getPage,'relatedPages' => $relatedPages,'kivs' => $kivs,'type' =>$type,'canonical_url' => $canonical_url]);
    }

    protected function products($getPage, $relatedPages,$canonical_url)
    {

        $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->where('ptr.page_id', $getPage->tid)
            ->whereIn('pages.template_id',[21])
            ->select('ptr.*','pt.slug as page_slug','products.id')
            ->with('media')
            ->get();

        return view("web.products", ['page' => $getPage,'relatedPages' => $relatedPages,'products' =>$products,'canonical_url' => $canonical_url]);
    }

    protected function faq($getPage, $relatedPages,$canonical_url)
    {
        $faqs = Faq::select('title_'.$this->lang.' as question','description_'.$this->lang.' as answer')->get();

        return view("web.faq", ['page' => $getPage,'relatedPages' => $relatedPages,'faqs' => $faqs,'canonical_url' => $canonical_url]);
    }

    protected function news($getPage, $relatedPages,$canonical_url)
    {
        $articles = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->where('articles.status', 1)
            ->where('at.page_id', $getPage->tid)
            ->where('at.lang', $getPage->lang)
            ->whereNull('pt.deleted_at')
            ->whereNull('at.deleted_at')
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->with('media')
            ->select('articles.id','articles.id','at.article_id','at.name',
                'at.slug', 'at.summary', 'articles.published_at',
                'pt.name as page_name', 'pt.slug as page_slug')
            ->paginate(9);

        return view("web.news", ['page' => $getPage,'articles' =>$articles,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function certificates($getPage, $relatedPages,$canonical_url)
    {
        $certificates = Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.lang',$this->lang)
            ->where('pages.template_id',5)
            ->where('pt.parent_id',$getPage->tid)
            ->select('pt.name','pt.content','pages.id','pages.no')
            ->with('media')
            ->paginate(12);

        return view("web.certificates", ['page' => $getPage,'relatedPages' => $relatedPages,'certificates' => $certificates,'canonical_url' => $canonical_url]);
    }

    protected function vacancies($getPage, $relatedPages,$canonical_url)
    {
        $vacancies = Vacancy::join('vacancy_translations as vt','vt.vacancy_id','vacancies.id')
            ->where('vt.lang',$this->lang)
            ->where('vacancies.end_date', '>=', date('Y-m-d'))
            ->select('vt.*','vacancies.published_by as start_date','vt.id as tid','vacancies.id')
            ->get();

        return view("web.vacancies", ['page' => $getPage,'relatedPages' => $relatedPages,'vacancies' => $vacancies,'canonical_url' => $canonical_url]);
    }

    protected function getPageByTemplateId($templateId)
    {
        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->where('pt.lang', $this->lang)
            ->where('pages.template_id', $templateId)
            ->select('pt.slug', 'pt.name', 'pt.id')
            ->first();

        return $page;
    }

    private function getChildrenPage($page)
    {
        $children = $page->children;

        if($children->count() && $page->id != $children->first()->id){

            return $children->first();
        }
        else{
            return $page;
        }
    }

    protected function similarArticles($id = 0,$template_id,$limit)
    {
        $articles = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->select('at.name', 'at.slug as article_slug','pt.slug','articles.published_at','articles.id')
            ->where('articles.id', '<>', $id)
            ->where('pages.template_id',$template_id)
            ->where('at.lang', $this->lang)
            ->where('articles.status', 1)
            ->whereNull('pt.deleted_at')
            ->whereNull('at.deleted_at')
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->limit($limit);

        return $articles;
    }

    protected function similarProducts($id = 0,$template_id,$limit,$page_id)
    {
        $products = Product::join('product_translations as ptr', 'products.id', '=', 'ptr.product_id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->where('products.id', '<>', $id)
            ->whereNull('pt.deleted_at')
            ->whereNull('ptr.deleted_at')
            ->where('pt.lang', $this->lang)
            ->where('ptr.lang', $this->lang)
            ->where('pages.template_id',$template_id)
            ->where('ptr.page_id',$page_id)
            ->with('media')
            ->select('ptr.*', 'ptr.id as tid', 'products.id','products.template_id','pt.slug as page_slug')
            ->inRandomOrder()
            ->limit($limit);

        return $products;
    }

    protected function dropdown($getPage,$relatedPages)
    {
        if ($getPage->MenuChildren('tid')->count()) {
            return redirect()->route('showPage', $getPage->MenuChildren('tid')->first()->slug);
        } else {
            return redirect()->route('home');
        }
    }
}
