<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Cellphone implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(array_key_exists($this->data['phone_prefix'], config('config.prefix-number'))){
            return strlen($value) === 7;
        }
        else{
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Telefon nömrəsi düzgün deyil!';
    }
}
