<?php

namespace App\Crud;

class ConfigCrud extends RenderCrud
{
    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Dəyər",
                "db" => "value",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
            ],

        ];

        return $this->render($fields, $action, $data);
    }

}


