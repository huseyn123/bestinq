<?php

namespace App\Crud;

use App\Logic\MultiLanguageSelect;
use App\Models\Page;

class VacancyCrud extends RenderCrud
{



    public function fields($lang, $action, $data = null)
    {
        $fields = [
            [
                "label" => 'Vəzifə',
                "db" => "position",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Dil",
                "db" => "lang",
                "type" => "select",
                "data" => config('app.locales'),
                "selected" => array_first(config('app.locales')),
                "attr" => ['class'=>'form-control'],
                "edit" => false,
                "divClass" => "language-form"
            ],
            [
                "label" => "Imumi Məlumat",
                "db" => "work_information",
                "type" => "textarea",
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor'.$lang]
            ],
        ];

        if(is_null($lang)){
            $paramFields = (new VacancyParameterCrud())->fields('get');
            return $this->render(array_merge($fields, $paramFields), $action, $data);
        }
        else{
            return $this->render($fields, $action, $data);
        }
    }
}


