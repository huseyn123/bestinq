<?php

namespace App\Crud;

use App\Models\Page;

class KivVideoCrud extends RenderCrud
{



    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Kover Şəkil",
                "db" => "image",
                "type" => "file",
                "show" => true,
                "attr" => ['class'=>'form-control image', 'style' => 'display:none', 'title' => 'Ölçü: 345x255'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false && $data->getFirstMedia()){
                        $title='Yenilə';
                        $img = '<div class="input-group"><img src="'.asset($data->getFirstMedia()->getUrl('thumb')).'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => "Youtube Link",
                "db" => "youtube_id",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


