<?php

namespace App\Crud;

class VacancyParameterCrud extends RenderCrud
{
    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => "Başlanma tarixi",
                "db" => "published_by",
                "type" => "text",
                "attr" => ['class'=>'form-control datepicker', 'autocomplete' => 'off', 'required']
            ],
            [
                "label" => "Bitmə tarixi",
                "db" => "end_date",
                "type" => "text",
                "attr" => ['class'=>'form-control datepicker', 'autocomplete' => 'off', 'required']
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}


