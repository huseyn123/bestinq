<?php

namespace App\Crud;

class BranchElementCrud extends RenderCrud
{

    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }


    public function fields($action, $data = null)
    {
        $fields = [

            [
                "label" => " ",
                "db" => "type",
                "type" => "hidden",
                "value" => $this->type,
            ],

        ];
        return $this->render($fields, $action, $data);
    }


}


