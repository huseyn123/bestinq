<?php

namespace App\Crud;

use Collective\Html\FormFacade;

abstract class RenderCrud
{
    protected function render($fields, $action, $data = null, $disabled = false)
    {
        if($action == 'show'){
            return $this->show($fields, $action, $data);
        }
        elseif($action == 'profile'){
            return $this->profile($fields, $action, $data, $disabled);
        }
        elseif($action == 'get'){
            //gets field as array
            return $fields;
        }
        else{
            return $this->edit($fields, $action, $data);
        }
    }


    private function renderForm($field, $data, $disabled = false)
    {
        if($disabled == true){
            $field['attr']['disabled'] = 'disabled';
        }

        if($field['type'] == 'select'){
            if(isset($field['attr']['multiple'])){
                $form = FormFacade::select($field['db'], $field['data'], $field['selected'], $field['attr']);
            }
            else{
                $form = FormFacade::select($field['db'], $field['data'], $data == false ? $field['selected'] : old($field['db'], $data->{$field['db']}), $field['attr']);
            }
        }
        else if($field['type'] == 'file'){
            $form = FormFacade::file($field['db'], $field['attr']);
        }
        else if($field['type'] == 'checkbox'){
            $form = FormFacade::checkbox($field['db'], $field['value'], $data == false ? false : $data->{$field['db']} , $field['attr']);
        }
        else if($field['type'] == 'password'){
            $form = FormFacade::password($field['db'], $field['attr']);
        }
        else if($field['type'] == 'hidden'){
            $form = FormFacade::hidden($field['db'], $field['value']);
        }
        else{
            $form = FormFacade::{$field['type']}($field['db'], $data == false ? null : old($field['db'], $data->{$field['db']}), $field['attr']);
        }

        if(isset($field['design'])){
            $render = call_user_func($field['design'], $form, $data);
        }
        else{
            $render = $form;
        }

        return $render;
    }


    private function edit($fields, $action, $data)
    {
        $html = '';

        if(!request()->ajax()){
            $class = 'form-group form-class col-md-12';
            $class2 = '';
            $class3 = '';
        }
        else{
            $class = 'form-group form-class';
            $class2 = 'col-md-8';
            $class3 = 'col-md-3 control-label';
        }

        foreach ($fields as $field){

            if(!isset($field[$action]) || $field[$action] == true){
                $html.='<div class="'.$class.' '.@$field['divClass'].' hh'.@implode(' hh', $field['hide']).'">';

                $label = FormFacade::label($field['db'], $field['label'], ['class' => $class3]);

                $form = $this->renderForm($field, $data);

                $html.=$label;

                $html.='<div class="'.$class2.'">';

                $html.=$form;

                if(isset($field['attr']['title'])){
                    $html.='<p class="help-block">'.$field['attr']['title'].'</p>';
                }

                $html.='</div></div>';
            }
        }

        return $html;
    }


    private function show($fields, $action, $data)
    {
        $html = '';

        foreach ($fields as $field){

            $html.='<tr>
                        <td><label for="">'.$field['label'].'</label> </td>
                        <td>'.$data[$field['db']].'</td>
                </tr>';

        }

        return $html;
    }


    private function profile($fields, $action, $data, $disabled)
    {
        $html = '';

        foreach ($fields as $field)
        {
            if(!isset($field[$action]) || $field[$action] == true){
                $label = '<label for="'.$field['db'].'" class="col-sm-2 control-label">'.$field['label'].'</label>';
                $input = $this->renderForm($field, $data, $disabled);

                $html.='<div class="form-group">'.$label.'<div class="col-sm-10">'.$input.'</div></div>';
            }
        }

        return $html;
    }
}