<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;
use App\Models\Page;

class CertificateParameterCrud extends RenderCrud
{

    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false && $data->getFirstMedia()){
                        if(substr($data->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                            $url = $data->getFirstMedia()->getFullUrl();
                            $style = 'width:100px;height:100px';
                        }
                        else{
                            $url = $data->getFirstMedia()->getUrl('thumb');
                            $style="max-width:100%";
                        }
                        $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';

                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control with_image" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => "No",
                "db" => "no",
                "type" => "text",
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => " ",
                "db" => "visible",
                "type" => "hidden",
                "value" => 1,
            ],
            [
                "label" => " ",
                "db" => "template_id",
                "type" => "hidden",
                "value" => 5,
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}


