<?php

return [
    'status' => ['Gizli', 'Aktiv'],
    'application_status' => ['Təsdiq et','Təsdiqlənib'],
    'alert' => ['danger', 'success'],
    "slider-type" => [1 => "Slider", "Kompaniya"],
    "article-status" => ["Qaralama", "Dərc olunub"],
    "building_status" => ["Köhnə tikili", "Yeni tikili"],
    "room_count" => [1=>1,2,3,4,5],
    "label" => ["danger", "success"],
    "menu-target" => [1 => "Self", 0 => "Blank"],
    "menu-visibility" => [0 => "Hidden","Header and Footer", "Header", "Footer","Footer2"],
    "menu-visibility-boolean" => [0 => "Gizli", "Aktiv"],
    "menu-visibility-label" => [0 => "danger", "success", "warning", "warning", "info"],
    "role" => [1 => "Administrator", "Editor"],
    "blog_page" => [0 => "No", "Yes"],

    "social-network" => ['facebook','instagram','youtube'],
    "social-network-class" => ['facebook' =>'facebook','instagram' => 'instagram','youtube' => 'youtube-play'],



    "subscriber-label" => ["0" => "warning", "1" => "success"],
    "subscriber-status" => ["0" => "pending", "1" => "active"],
    "yes-no" => [0 => "minus", "plus"],
    'prefix-number' => [50 => 50, 51 => 51, 55 => 55, 70 => 70, 77 => 77],
    'slug_replacement' => ['az' => 'azerbaijani', 'en' => 'azerbaijani', 'ru' => 'russian'],
    'lang' => ['az' => 'AZ', 'en' => 'EN', 'ru' => 'RU'],


    'employee_count' => ['1-10','10-20','20-50','50-100'],

    "model" => ['articles' => 'App\Models\Article','page' => 'App\Models\Page', 'pageTranslation' => 'App\Models\PageTranslation','products' => 'App\Models\Product', 'productTranslation' => 'App\Models\ProductTranslation', 'articleTranslation' => 'App\Models\ArticleTranslation'],

    "template" => [
        1 =>'Dropdown',
        2 =>'Blok',
        3 =>'Xəbərlər səhifəsi',
        4 =>'Əlaqə',
//      5 Certificate
        6 =>'Şirkət Haqqında',
        7 =>'Keyfiyyət siyasəti || Korporativ sosial məsuliyyət',
        8 =>'Məhsulun qaytarılması',
        9 =>'Məmnuniyyət Anketi',
        10 =>'Məxfilik siyasəti',
        11 =>'Saytdan istifadə qaydaları',
        12 =>'Müştəri hüquqları',
        13 =>'Şirkət Rəhbərinin müraciəti',
        14 =>'Missiya və dəyərlərimiz',
        15 =>'Vakansiyalar',
        16 =>'Tərəfdaş ol & Topdan satış',
        17 =>'Əhatə dairəmiz',
        18 =>'Faq',
        19 =>'Sertifikatlar',
        20 =>'Kiv üçün',
        21 =>'Məhsullar Səhifəsi',
        22 =>'Bloq Yazilari səhifəsi',
    ],

    "product_template" => [
        15 => 'Çörək',
        16 => 'Şirniyyat',
        17 => 'Şokolad',
     ],

    'filter-type' => ['Bütün', 'Aktiv', 'Silinən'],
    'custom-apartment-type' => ['Bütün', 'Aktiv', 'Silinən'],

    'kiv_types' => [
        'images' =>'Şirkətdən görüntülər',
        'logos' =>'Şirkətin loqosu',
        'videos' =>'Reklam çarxları'
    ],

    'kiv_types_sitemap' => [
        'images' =>1133,
        'logos' =>4466,
        'videos' =>7799
    ],

    'kiv_type_database' => [
        'logos' =>1,
        'videos' =>2
    ],

    'kiv_class' => [
        'images' => 'company_image',
        'logos' => 'company_logo',
        'videos' => 'adv_video_page',
    ],

    'gallery-side' => [1 => 'left', 'right', 'center'],
    'gallery_size' =>[
        20 =>['width' => '345','height'=>'255'],
    ],
     'page_size'  =>[
         2 =>['width' => '720','height'=>'430'],
         5 =>['width' => '360','height'=>'470'],
         6 =>['width' => '720','height'=>'430'],
         13 =>['width' => '720','height'=>'865'],
         14 =>['width' => '720','height'=>'585'],

     ],
    'thumb_page_size'  =>[
        2 =>['width' => '200','height'=>'120'],
        5 =>['width' => '270','height'=>'350'],
        6 =>['width' => '200','height'=>'120'],
        13 =>['width' => '200','height'=>'245'],
        14 =>['width' => '200','height'=>'165'],
    ],

    'page_size2'  =>[
        4 =>['width' => '720','height'=>'555'],
    ],
    'thumb_page_size2'  =>[
    ],

    'crud'  =>[
        'articles-list' =>['width' => '720','height'=>'525'],
    ],

];
