<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Admin;
use App\Models\Config;
use App\Models\Dictionary;
use App\Models\Cities;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('AdminSeeder');
        $this->call('SettingsSeeder');

        $path = 'app/Sql/dictionaries.sql';

        if(Dictionary::count() == 0){
            DB::unprepared(file_get_contents($path));

            $this->command->info('Dictionary table seeded!');
        }



        Model::reguard();
    }
}

class AdminSeeder extends Seeder
{
    public function run()
    {
        if(Admin::count() == 0){
            Admin::create([
                "name" => "Huseyn Huseynli",
                "email" => "huseyn.h@gmail.com",
                "password" => bcrypt("123456"),
            ]);
        }
    }
}

class SettingsSeeder extends Seeder
{
    public function run()
    {
        if(Config::count() == 0){
            Config::create(['key' => 'name', 'config_path' => 'app.name']);
            Config::create(['key' => 'url', 'config_path' => 'app.url']);
            Config::create(['key' => 'lang', 'value' => config('app.locale')]);

            Config::create(['key' => 'host', 'config_path' => 'mail.host']);
            Config::create(['key' => 'port', 'config_path' => 'mail.port']);
            Config::create(['key' => 'encryption', 'config_path' => 'mail.encryption']);
            Config::create(['key' => 'username', 'config_path' => 'mail.username']);
            Config::create(['key' => 'password', 'config_path' => 'mail.password']);
            Config::create(['key' => 'smtp_address', 'config_path' => 'mail.from.address']);
            Config::create(['key' => 'smtp_title', 'config_path' => 'mail.from.name']);

            Config::create(['key' => 'google_api_key']);
            Config::create(['key' => 'google_analytic_key']);
            Config::create(['key' => 'google_analytic_view_id', 'config_path' => 'analytics.view_id']);

            Config::create(['key' => 'email']);
            Config::create(['key' => 'contact_email']);
            Config::create(['key' => 'contact_phone']);
            Config::create(['key' => 'contact_phone2']);
            Config::create(['key' => 'location']);
            foreach (config('config.social-network') as $data){
                Config::create(['key' => $data]);
            }
            Config::create(['key' => 'product1_count']);
            Config::create(['key' => 'product2_count']);
            Config::create(['key' => 'product3_count']);
            Config::create(['key' => 'video_link']);
        }
    }
}
