<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailToProductTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_translations', function (Blueprint $table) {
            $table->string('ingredients', 1000)->nullable()->after('content');
            $table->string('temperature')->nullable()->after('content');
            $table->text('use_amount')->nullable()->after('content');
            $table->string('storage_date')->nullable()->after('content');
            $table->string('storage_condition')->nullable()->after('content');
            $table->string('use_rule',1000)->nullable()->after('content');
            $table->string('cooking')->nullable()->after('content');
            $table->string('weight')->nullable()->after('content');
            $table->string('first_screaming')->nullable()->after('content');
            $table->string('last_screaming')->nullable()->after('content');
            $table->string('fertilize_time')->nullable()->after('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_translations', function (Blueprint $table) {
            $table->dropColumn('ingredients');
            $table->dropColumn('temperature');
            $table->dropColumn('use_amount');
            $table->dropColumn('storage_date');
            $table->dropColumn('storage_condition');
            $table->dropColumn('use_rule');
            $table->dropColumn('cooking');
            $table->dropColumn('weight');
            $table->dropColumn('first_screaming');
            $table->dropColumn('last_screaming');
            $table->dropColumn('fertilize_time');
        });
    }
}
