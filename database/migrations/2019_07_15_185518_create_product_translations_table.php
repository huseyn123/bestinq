<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('page_id')->nullable();
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('summary', 1000)->nullable();
            $table->text('content')->nullable();
            $table->string('lang', 2);
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['product_id', 'lang']);

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('page_id')->references('id')->on('page_translations')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_translations');
    }
}
