<!DOCTYPE html>
<html lang="{{$lang}}">
<head>
    <title>@if(!is_null($page_heading)){{ $page_heading }} | @endif{{ $config['name'] }}</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover">
    <meta name="theme-color" content="#ffffff">
    <meta property="author" content="{{ $config['name'] }}">
    <meta property="og:title" content="{{ $page_heading }}">
    <meta property="og:url" content="{{request()->url()}}">
    <meta property="og:image" content="{{isset($page_image) ? asset($page_image):  asset('images/logo.svg')}}">
    @if(isset($other_page))
        <meta name="keywords" content="{{ $other_page->meta_keywords }}">
        <meta name="description" content="{{ $other_page->meta_description }}">
        <meta property="og:description" content="{{ $other_page->meta_description }}">
    @elseif(isset($page))
        <meta name="keywords" content="{{ $page->meta_keywords }}">
        <meta name="description" content="{{ $page->meta_description }}">
        <meta property="og:description" content="{{ isset($page_description) ? strip_tags($page_description) : @$page->meta_description }}">
    @endif
    @if(!isset($index_page))
        <link rel="canonical" href="{{ $canonical_url ?? ''}}" />
    @endif

    @if(isset($relatedPages))
        @foreach($relatedPages as $key=>$value)
            <link rel="alternate" href="{{ url($value) }}" hreflang="{{$key}}" />
        @endforeach
    @endif
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicon/fav_180.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/fav_32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/fav_16.png') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset(mix('css/style.css')) }}" rel="stylesheet">
    <!-- GOOGLE FONTS -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        MAPS_API_KEY = '{{$config['google_api_key']}}';
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NPB246K');</script>
    <!--EndGoogleTagManager -->

    @include('widgets.analytic', ['code' => $config['google_analytic_key'] ])
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NPB246K"  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <main>
        @include('web.elements.header')
            @yield('content')
        @include('web.elements.footer')
    </main>
    <script src="{{asset(mix('js/script.js'))}}"></script>
    @stack('scripts')
</body>
</html>
