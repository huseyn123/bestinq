@extends ('layouts.web', ['page_heading' => $user->name ] )

@section ('content')
    <!-- Account Begin -->
    <section class="account">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xs-12 accounth">
                    <!-- AccountSide -->
                    <aside class="account_side">
                        <div class="profile">
                            <figure>
                                <img src="{{ asset('images/profile.png') }}" alt="">
                            </figure>
                            <span class="name">{{ $user->name }}</span>
                        </div>
                        <ul class="list-unstyled side_menu">
                            <li {{ activeUrl(route('user.profile')) }}><a href="{{ route('user.profile') }}" title="">Şəxsi məlumatlar</a></li>
                            <li class="line"></li>
                            <li {{ activeUrl(route('user_app.all')) }}><a href="{{ route('user_app.all') }}" title="">Müraciətlər</a></li>
                            {{--<li class="line"></li>--}}
                            {{--<li {{ activeUrl(route('user_app.index')) }}><a href="{{ route('user_app.index') }}" title="">Müraciət forması</a></li>--}}
                        </ul>
                    </aside>
                </div>


                <div class="col-md-10 col-xs-12 accounth">
                    <!-- AccountContent -->
                    <div class="account_content">
                        @yield('section')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Account End -->

@endsection

