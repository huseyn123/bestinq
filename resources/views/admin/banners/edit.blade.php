@extends('layouts.modal', ['route' => $route, 'method' => 'PUT','script' => true])
@section('title', $data->name ?? $title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">


        {!! $fields !!}



        <div class="form-group">

            {!! Form::label('image', 'Şəkil',['class' => 'col-md-3 control-label']) !!}


            <div class="col-md-8">
                <ul class="nav nav-tabs">
                    @foreach(config("app.locales") as $key => $locale)
                        <li role="presentation" @if($loop->first) class="active" @endif>
                            <a href="#{{$key.'image'}}" data-toggle="tab">{{$locale}}</a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content">

                    @foreach(config("app.locales") as $key => $locale)

                        <div id="{{$key.'image'}}" class="tab-pane fade @if($loop->first) in active @endif">

                            <div class="input-group" style="padding-top: 20px">

                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                        <i class="fa fa-cloud-upload"></i>@if(isset($data) && $data->getFirstMedia('default_'.$key)) Yenilə @else Əlavə et @endif<input class="form-control image" style="display:none" name="image_{{$key}}" type="file" id="image">
                                    </span>
                                </label>

                                <input type="text" class="form-control" readonly="" required="">

                            </div>
                            <div class="divImage" >
                                <img class="showImage" src="@if(isset($data) && $data->getFirstMedia('default_'.$key)){{ asset($data->getFirstMedia('default_'.$key)->getUrl('thumb')) }}@endif">
                            </div>

                        </div>

                    @endforeach

                </div>
            </div>

        </div>



    </div>


@endsection
