@extends ('layouts.modal', ['script' => true])
@section ('title',$create_title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">


        {!! $fields !!}


        <div class="form-group">

            {!! Form::label('image', 'Şəkil',['class' => 'col-md-3 control-label']) !!}


            <div class="col-md-8">
                <ul class="nav nav-tabs">
                    @foreach(config("app.locales") as $key => $locale)
                        <li role="presentation" @if($loop->first) class="active" @endif>
                            <a href="#{{$key.'image'}}" data-toggle="tab">{{$locale}}</a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content">

                    @foreach(config("app.locales") as $key => $locale)

                        <div id="{{$key.'image'}}" class="tab-pane fade @if($loop->first) in active @endif">

                            <div class="input-group" style="padding-top: 20px">

                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                        <i class="fa fa-cloud-upload"></i> Əlavə et<input class="form-control image" style="display:none" name="image_{{$key}}" type="file" id="image">
                                    </span>
                                </label>

                                <input type="text" class="form-control" readonly="" required="">

                                <div class="divImage" style="display:none">
                                    <img class="showImage" src="#">
                                </div>

                            </div>

                         </div>

                    @endforeach

                </div>
            </div>

        </div>


    </div>

@endsection
