@extends ('layouts.modal', ['script' => true])
@section ('title', $create_title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @include('widgets.lang-tab', ['input' => 'text', 'name' => 'location','tab_title' => 'Ünvan'])

        {!! $fields !!}



    </div>

@endsection
