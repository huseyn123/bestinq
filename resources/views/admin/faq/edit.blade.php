@extends('layouts.modal', ['route' => $route, 'method' => 'PUT','script' => true])
@section('title', $data->name ?? $title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @include('widgets.lang-tab', ['input' => 'text', 'info' => $data,'name' => 'title'])
        @include('widgets.lang-tab', ['input' => 'textarea', 'info' => $data,'name' => 'description'])


    </div>


@endsection
