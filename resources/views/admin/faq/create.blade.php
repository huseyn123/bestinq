@extends ('layouts.modal', ['script' => true])
@section ('title', 'Yeni Faq')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @include('widgets.lang-tab', ['input' => 'text', 'name' => 'title','tab_title' => 'Sual'])

        @include('widgets.lang-tab', ['input' => 'textarea', 'name' => 'description','tab_title' => 'Cavab'])

    </div>

@endsection
