@extends ('layouts.modal', ['script' => true])
@section ('title',$create_title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @if(isset($lang_tab))
            @include('widgets.lang-tab', ['input' => 'text', 'name' => 'title','tab_title' => 'Ad'])
        @endif

        {!! $fields !!}

    </div>

@endsection
