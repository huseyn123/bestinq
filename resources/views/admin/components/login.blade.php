<div class="form-group has-feedback @if($errors->has('email')) has-error @endif">
    @if ($errors->has('email'))
        <label>{{ $errors->first('email') }}</label>
    @endif
    <input type="email" name="email" class="form-control" placeholder="Email" required>
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>
<div class="form-group has-feedback @if($errors->has('email')) has-error @endif">
    <input type="password" class="form-control" placeholder="Password" name="password" required>
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>
<div class="row">
    <div class="col-xs-8">
        <div class="checkbox icheck">
            <label>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ trans('admin.login_remember') }}
            </label>
        </div>
    </div>
    <!-- /.col -->
    <div class="col-xs-4">
        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('admin.login_sign_in') }}</button>
    </div>
    <!-- /.col -->
</div>