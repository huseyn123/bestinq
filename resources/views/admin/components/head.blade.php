<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ getConfig()['name'] }} | @yield('title')</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicon/fav_180.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/fav_32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/fav_16.png') }}">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
<link href="{{ asset('css/admin/skin-blue.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/editor/css/dataTables.editor.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/editor/css/editor.bootstrap.css') }}">

@stack('styles')
<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic&text=%21%22%23%24%25%26%27%28%29%30+,-./0123456789:;%3C=%3E%3F@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`%E2%82%AC„‘’“”™©®°µ±÷abcdefghijklmnopqrstuvwxyz{|}~%C3%9C%C3%96%C4%9E%C4%B0%C6%8F%C3%87%C5%9E%C3%BC%C3%B6%C4%9F%C4%B1%C9%99%C3%A7%C5%9F">
