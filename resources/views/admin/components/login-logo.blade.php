<div class="login-logo">
    <a href="{{ url('/') }}">
        <img src="{{asset("images/logo.svg")}}" style="max-width:100%">
    </a>
</div>
@if (session()->has('status'))
    <div class="alert alert-success">
        {{ session()->get('status') }}
    </div>
@endif