@extends ('layouts.admin')
@section ('title', $title)

@section('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'subscribers.create', 'route' => 'subscribers'])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'subscribers', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    <script src="{{ asset('vendor/dataTables/buttons.server-side.js') }}"></script>

    {!! $dataTable->scripts() !!}

@endpush