@extends ('layouts.admin', ['table' => 'pages'])
@section ('title', $title)

@section('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => $route.'.create', 'route' => $route, 'filter' => config('config.filter-type'), 'largeModal' => true, 'locale' => true])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'pages', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush