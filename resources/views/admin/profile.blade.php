@extends('layouts.admin')
@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    @include('widgets.profile-picture', ['class' => 'profile-user-img img-responsive img-circle'])
                    <h3 class="profile-username text-center">{{ Auth::guard('admin')->user()->name }}</h3>
                    <p class="text-muted text-center">Software Engineer</p>
                    {!! Form::open(['url' => route('admins.updatePhoto'), 'method' => 'PUT', 'files' => true]) !!}
                        <div class="form-group">
                            <input type="file" name="photo">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block"><b>Şəkli dəyiş</b></button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#settings" data-toggle="tab">Şəxsi məlumatlar</a></li>
                    <li><a href="#password" data-toggle="tab">Şifrə</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="settings">
                        {!! Form::open(['url' => route('admins.update', Auth::guard('admin')->user()->id), 'method' => 'PUT', 'class'=>'form-horizontal', 'id' => 'dtForm']) !!}
                            {!! $fields !!}

                            @if($disabled == false)
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        {!! Form::button(trans('locale.save'), ['class' => 'btn btn-danger loadingButton', 'type' => 'submit', 'data-loading-text' => loading()]) !!}
                                    </div>
                                </div>
                            @endif

                            {!! Form::hidden('type', 2) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="tab-pane" id="password">
                        {!! Form::open(['url' => route('admins.updatePassword'), 'method' => 'PUT', 'class'=>'form-horizontal dtForm']) !!}
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Şifrə</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="current_password" type="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Yeni şifrə</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="password" type="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Yeni şifrənin təkrarı</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="password_confirmation" type="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    {!! Form::button(trans('locale.save'), ['class' => 'btn btn-danger loadingButton', 'type' => 'submit', 'data-loading-text' => loading()]) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

