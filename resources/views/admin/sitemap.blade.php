@extends ('layouts.admin')
@section ('title', $title)
@section ('content')

    @if(!is_null($date->value))
        <div class="alert alert-success">
            Son yenilənmə: {{ filterDate($date->value, 'true', 'eFull') }}
            <a href="{{ url('storage/sitemap.xml') }}" class="alert-link pull-right" target="_blank">Sitemap faylını göstər</a>
        </div>
    @endif

    {!! Form::open(['url'=>route('sitemap.store'), 'method'=>'POST', 'class' => 'text-center']) !!}
    @include('widgets.form-submit', ['text' => 'Yenilə', 'class' => 'success', 'icon' => 'refresh'])
    {!! Form::close() !!}

@endsection
