@extends ('layouts.admin')
@section ('title', $title)

@section ('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'partners.create', 'route' => 'partners'])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'partners', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush