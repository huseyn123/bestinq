@extends ('layouts.admin', ['table' => 'products'])
@section ('title', $title)

@section('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'products.create', 'route' => 'products', 'filter' => config('config.filter-type'), 'largeModal' => true, 'locale' => true])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'products', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush