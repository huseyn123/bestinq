@extends ('layouts.admin', ['table' => 'vacancies'])
@section ('title', $title)

@section('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'vacancies.create','filter' => config('config.filter-type'),'route' => 'vacancies', 'largeModal' => true, 'locale' => true,])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'vacancies', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush