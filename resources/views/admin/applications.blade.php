@extends ('layouts.admin', ['table' => 'applications'])
@section ('title', $title)

@section('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'applications.create', 'route' => 'applications', 'filter' => config('config.filter-type'), 'custom_filter' => config('config.filter-type2'), 'largeModal' => true, 'locale' => true])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'applications', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    <script>

        $('#custom_filter').on('change', function(e) {
            $('#applications').DataTable().draw(false);
            e.preventDefault();
        });

        $("#applications").on('preXhr.dt', function(e, settings, data) {
            data.app_amount = $('#custom_filter').find("option:selected").val();
        });
    </script>

    {!! $dataTable->scripts() !!}
@endpush