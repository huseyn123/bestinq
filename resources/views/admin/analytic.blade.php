@extends ('layouts.admin')
@section ('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Konfiqurasiya (1-ci mərhələ)</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(['route' => 'analytic.store', 'method' => 'POST', 'role' => 'form', 'files' => true]) !!}
                <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Analytic View ID</label>
                            <input type="text" class="form-control" name="view_id" value="{{ old('view_id', getConfig()['google_analytic_view_id']) }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Service Account JSON file</label>
                            <input type="file" name="file">

                            <p class="help-block">Bunun nə olduğunu bilmirsinizsə, boş buraxın.<br></p>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Quraşdır</button>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Konfiqurasiya (2-ci mərhələ)</h3>
                </div>
                <br>
                <div class="col-md-12">
                    <p class="text-muted well well-sm no-shadow">
                        Əgər siz öz faylınızı yükləmisinizsə, həmən faylda "client_email" ünvanını copy edin.<br><br>
                        Əgər faylı yükləməmisinizsə, bu ünvanı copy edin:<br>
                        laravel-analytics-new@api-project-16320660296.iam.gserviceaccount.com<br><br>

                        Google Analytics Hesabınıza daxil olun, admin bölməsində User Management hissəsinə copy etdiyiniz ünvanı daxil edin. İcazə kimi "Read and Analyze" kifayət edər.
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
