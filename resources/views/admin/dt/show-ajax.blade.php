@extends ('layouts.modal', ['script' => $script ?? true])
@section ('title', $title)

@section('content')
    @include('widgets.modal-confirm')

    {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'table', 'width' => '100%'], true) !!}
@endsection

{!! $dataTable->scripts() !!}

