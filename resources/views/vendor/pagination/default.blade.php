@if ($paginator->hasPages())
    <div class="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())

        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="prev"><i class="icon-arrow-left8"></i> Previous Posts</a>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="next"><i class="icon-arrow-right8"></i></a>
        @else

        @endif
    </div>
    <div class="clearfix"></div>
@endif
