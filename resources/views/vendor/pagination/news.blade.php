@if ($paginator->hasPages())

    <div class="pagination">

        <!-- Pagination Begin -->
            <ul >
                {{-- Previous Page Link --}}
                @if (!$paginator->onFirstPage())
                    <li><a  class="disabled prev" href="{{ $paginator->previousPageUrl() }}"><i class="fa fa-angle-left"></i></a></li>
                @endif
                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="active"><a >{{ $page }}</a></li>
                            @else
                                <li><a href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li ><a  class="next" href="{{ $paginator->nextPageUrl() }}"><i class="fa fa-angle-right"></i></a></li>
                @endif
            </ul>

    </div>

@endif

