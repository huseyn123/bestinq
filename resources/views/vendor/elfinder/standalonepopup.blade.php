@extends ('planes.app')
@section('main-title', 'File Manager')
@section ('layout')
    <div id="wrapper">
        <div class="row">
                <div id="elfinder"></div>
                <br>
        </div>
    </div>
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/elfinder/css/elfinder.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/elfinder/css/theme.css') }}">
@endpush

@if(request()->get('files') == true)
    @php $mimes = json_encode( explode(',', request()->get('files')) ); @endphp
@else
    @php $mimes = json_encode(['image']); @endphp
@endif

@push('scripts')

<script src="{{asset('vendor/elfinder/js/elfinder.min.js') }}"></script>

<script type="text/javascript">
    $().ready(function () {
        var elf = $('#elfinder').elfinder({
            onlyMimes: {!! $mimes !!},
            customData: {
                _token: '<?= csrf_token() ?>'
            },
            url: '<?= route("elfinder.connector") ?>',  // connector URL
            dialog: {width: 900, modal: true, title: 'Faylı seç'},
            resizable: false,
            commandsOptions: {
                getfile: {
                    oncomplete: 'destroy'
                }
            },
            getFileCallback: function (file) {
                window.parent.processSelectedFile(file.path, '<?= $input_id?>');
                parent.jQuery.colorbox.close();
            }
        }).elfinder('instance');
    });
</script>
@endpush

