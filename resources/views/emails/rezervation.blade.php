@component('mail::message')

    İstifadəçinin məlumatları:

    - Ad,Soyad: {{$content['fullname']}}


    @if(isset($content['email']))
        - Email: {{$content['email']}}
    @endif

    @if(isset($content['phone']))
        - Telefon: {{$content['phone']}}
    @endif

    @if(isset($content['entry_date']))
        - Giriş Tarixi: {{$content['entry_date']}}
    @endif

    @if(isset($content['output_date']))
        - Çıxış Tarixi: {{$content['output_date']}}
    @endif

    @if(isset($content['type']))
        - Otağ növü : {{$content['type']}}
    @endif

    @if(isset($content['adult']) && $content['adult'] > 0)
        - Böyük sayı : {{$content['adult']}}
    @endif

    @if(isset($content['child']) && $content['child'] > 0)
        - Uşaq sayı : {{$content['child']}}
    @endif

    @if(isset($content['feeding']))
        - Qidalanma : @foreach($content['feeding'] as $key=>$value) {{$dictionary[$key] ?? $value }} @if($loop->count > 1 && !$loop->last), @endif @endforeach
    @endif

@endcomponent