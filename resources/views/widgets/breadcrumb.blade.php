@if ($breadcrumbs)
    @foreach ($breadcrumbs as $breadcrumb)
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            @if (!$breadcrumb->last)
                <a itemprop="item" href="{{ $breadcrumb->url }}"><span itemprop="name"> {{ removeSymbol($breadcrumb->title ,'/') }} </span></a>
                <span itemprop="position" content="{{ $loop->iteration }}"><i class="fa fa-angle-right"></i></span>
            @else
                <a itemprop="item" ><span itemprop="name">{{$breadcrumb->title }}</span></a>
                <span itemprop="position" content="{{ $loop->iteration }}"></span>
            @endif
        </li>
    @endforeach
@endif
