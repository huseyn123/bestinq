
<a href="#" class="btn btn-xs btn-primary open-modal-dialog" data-link="{{ route("$route.edit", $row->tid) }}" data-large="{{ $largeModal ?? false }}"><i class="fa fa-edit"></i> </a>

<a href="#" class="btn btn-xs btn-danger" data-action="{{route("$route.destroy", $row->tid)}}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
