<div class="form-group">

    @if(isset($tab_title))
        {!! Form::label($name, $tab_title,['class' => 'col-md-3 control-label']) !!}
    @endif

    <div class="col-md-8">


        <ul class="nav nav-tabs">
            @foreach(config("app.locales") as $key => $locale)
                <li role="presentation" @if($loop->first) class="active" @endif>
                    <a href="#{{$key.$name}}" data-toggle="tab">{{$locale}}</a>
                </li>
            @endforeach
        </ul>

        <div class="tab-content">

            @foreach(config("app.locales") as $key => $locale)

                <div id="{{$key.$name}}" class="tab-pane fade @if($loop->first) in active @endif">

                    @if(!isset($multiRow))

                        @if(isset($editor))
                            {!! Form::$input($name.'_'.$key, isset($info) ? $info[$name.'_'.$key] : null, ['class' => 'form-control', 'id' => $editor == 1 ? "editor-$key" : 'no-edit']) !!}
                        @else
                            {!! Form::$input($name.'_'.$key, isset($info) ? $info[$name.'_'.$key] : null, ['class' => 'form-control']) !!}
                        @endif
                    @else
                        @if(isset($editor))

                            {!! Form::$input("content[$key]", isset($info[$key]) ? $info[$key] : null, ['class' => 'form-control' , 'id' => $editor == 1 ? "editor-$key" : 'no-edit']) !!}
                        @else
                            {!! Form::$input("content[$key]", isset($info[$key]) ? $info[$key] : null, ['class' => 'form-control']) !!}
                        @endif
                    @endif
                </div>
            @endforeach

        </div>
    </div>

</div>
