<div class="pull-right">
    <div class="form-group col-md-12" style="border-top:1px solid #eee"><br>
        {!! Form::button(icon($icon).' '.$text, ['class' => "loadingButton btn btn-$class", 'type' => 'submit', 'data-loading-text' => loading(), 'autocomplete' => 'off']) !!}

        @if(isset($return))
            {!! Form::button(trans('locale.save'), ['class' => 'btn btn-success loadingButton', 'type' => 'submit', 'data-loading-text' => loading()]) !!}
        @endif
    </div>
</div>