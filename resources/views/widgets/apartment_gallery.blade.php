@foreach(config('config.apartmentGalleryType') as $key=>$value)
    <p><a href="{{ route('gallery.index', [$route,$data->id]) }}?type={{$key}}"  @if($gallery_type == $key)style="text-decoration: underline" @endif>{{ $value }}</a></p>
@endforeach

