@extends('layouts.guest')
@section('title', trans('admin.password_reset'))
@section('content')

<div class="login-box">
    <div class="login-box-body">
        @include('admin.components.login-logo')
        <p class="login-box-msg">{{ trans('admin.password_reset') }}</p>
        <form method="post" action="{{ url($url ?? '/password/reset') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group has-feedback @if($errors->has('email')) has-error @endif">
                <input type="email" placeholder="Email" name="email" autofocus="autofocus" class="form-control" required>
                @if ($errors->has('email'))
                    <label>{{ $errors->first('email') }}</label>
                @endif
            </div>
            <div class="form-group has-feedback @if($errors->has('password')) has-error @endif">
                <input type="password" placeholder="Şifrə" name="password" autofocus="autofocus" class="form-control" required>
                @if ($errors->has('password'))
                    <label>{{ $errors->first('password') }}</label>
                @endif
            </div>
            <div class="form-group has-feedback @if($errors->has('password_confirmation')) has-error @endif">
                <input type="password" placeholder="Şifrəni təkrar daxil edin" name="password_confirmation" autofocus="autofocus" class="form-control" required>
                @if ($errors->has('password_confirmation'))
                    <label>{{ $errors->first('password_confirmation') }}</label>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-8">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Şifrəni dəyiş</button>
                </div>
                <div class="col-xs-2"></div>
            </div>
        </form>
    </div>
</div>

@endsection
