@extends('layouts.guest')
@section('title', trans('admin.login'))
@section('content')

<div class="login-box">
    <!-- /.login-logo -->
    <div class="login-box-body">
        @include('admin.components.login-logo')
        <p class="login-box-msg">{{ trans('admin.login_text') }}</p>
        <form action="{{ route('admin.login.submit') }}" method="post">
            @csrf
            @include('admin.components.login')
        </form>

        <a href="{{ route('admin.password.request') }}">{{ trans('admin.password_lost') }}</a><br>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@endsection