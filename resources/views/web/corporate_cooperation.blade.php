@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.breadcrumbs')

    <div class="for_media container">
        <div class="row">
            <nav id="left_navi" class="col-md-3">
                @include('web.elements.shop_navbar')
            </nav>
            <div class="col-md-9">
                <div class="form">
                    @include('web.form.form_succes')
                    <h3 class="title text-left">{{ $page->name }}</h3>

                    @include('web.form.'.$view)

                </div>
            </div>
        </div>
    </div>

@endsection
