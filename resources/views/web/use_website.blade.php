@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>



    <div class="container text_page">
        <h1 class="title">{{$page->seo_title}}</h1>
        {!! $page->content !!}
        <h3 class="title2">Veb səhifənin ümumi şərtləri</h3>

        @foreach($blocks as $block)
            <h4 class="title3">{{$block->name}}</h4>
            {!! $block->content !!}

        @endforeach

    </div>


@endsection