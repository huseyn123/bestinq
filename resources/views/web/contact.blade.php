@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>


    <div class="container contact_page">
        <div class="page_title">{{$dictionary['c_info'] ?? 'Əlaqə Məlumatları'}}</div>
        <div class="row group1">

            <div class="col-md-6">
                @if($page->getFirstMedia('cover'))
                    <div class="pro_img1">
                        <img src="{{$page->getFirstMedia('cover')->getUrl('blade')}}">
                    </div>
                @endif
            </div>

            <div class="col-md-6 group2">
                <ul class="type_list">
                    <li><b>{{$dictionary['ct1'] ?? 'Ünvan: '}} </b> {{$dictionary['address']}}</li>
                    <li><b>{{$dictionary['ct2'] ?? 'Telefon nömrəsi: '}} </b> {{$config['contact_phone']}}</li>
                    <li><b>{{$dictionary['ct3'] ?? 'E-mail: '}} </b> {{$config['contact_email']}}</li>
                </ul>
                <div class="text">
                    <div class="title">{{$dictionary['ct4'] ?? 'Hörmətli istifadəçi '}} </div>
                    {!! $page->content !!}
                </div>
            </div>
        </div>
        <div class="row group1">
            <div class="col-md-6">
                <div id="contact_maps" class="maps" data-maps="true" data-zoom="11" data-lat="{{ $config['location'] }}" data-lng="{{ $config['location'] }}"></div>
            </div>
            <div class="col-md-6">
                <div class="form main_ct_id">
                    @include('web.elements.succes_form')

                    @include('web.form.contact',['id' =>'ct_id'])
                </div>
            </div>
        </div>
    </div>


@endsection
