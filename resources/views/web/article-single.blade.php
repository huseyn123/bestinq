@extends ('layouts.web', ['page_heading' => $article->name,'other_page'=>$article] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs',['data' => $article])
    </div>

    <div class="blog_detail_page container">
        <div class="row">

            <div class="col-md-3 others mo-1">
                @include('web.elements.similarArticle')
            </div>

            <div class="col-md-9 detail mo-0">
                <h1 class="title">{{ $article->name }}</h1>
                <div class="date">{{ blogDate($article->published_at) }}</div>
                <div class="clearfix">
                    @if($article->getFirstMedia())
                        <div class="img"><img src="{{ asset($article->getFirstMedia()->getUrl('blade')) }}"/></div>
                    @endif
                    <div class="text">
                        {!! $article->content !!}
                    </div>
                </div>
                <div class="owl-carousel">

                    @foreach($article->getMedia('gallery') as $gallery)

                        <a data-fancybox="gallery" href="{{ $gallery->getUrl('blade') }}"><img src="{{ $gallery->getUrl('thumb') }}"></a>

                    @endforeach

                </div>

            </div>
        </div>
    </div>

@endsection

