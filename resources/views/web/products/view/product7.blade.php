<div class="container">
    <div class="page_title2">{{$product->name}}</div>
    <div class="pro_block1">
        <div class="b1">
            <b>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</b>
            <p>{{$product->ingredients}}</p>
        </div>

        <div class="b2">
            <b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b>{{ $product->use_rule }}
        </div>

        <div class="b3">
            @include('web.products.product_img')
        </div>
        <div class="b4">

            <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
            {!! $product->use_amount !!}

        </div>
    </div>
    <div class="row pro_group1 margin_bottom">
        <div class="col-md-3 mo-0">
            <div class="pro_img1"><img src="{{asset('storage/files/products/whole_wheat/whole_wheat_mix1.svg')}}"></div>
        </div>
        <div class="col-md-3 mo-2">
            <div class="pro_img1"><img src="{{asset('storage/files/products/whole_wheat/whole_wheat_mix2.svg')}}"></div>
        </div>
        <div class="col-md-3 mo-1">
            <ul class="pro_block3">
                <li><b>{{$dictionary['temperature'] ?? 'Xəmirin temperaturu:'}} {{$product->temperature}}</b>
                <li><b>{{$dictionary['cooking'] ?? 'Bişirmə:'}}</b><br>{{$product->cooking}}</li>
                <li><b>{{$dictionary['product_weight'] ?? 'Çəkisi:'}}</b><br>{{$product->weight}}</li>
            </ul>
        </div>
        <div class="col-md-3 mo-3">
            <ul class="pro_block3">
                <li><b>{{$dictionary['first_screaming'] ?? 'İlkin qıcqırma mərhələsi:'}} </b><br> {{$product->first_screaming}}</li>
                <li><b>{{$dictionary['last_screaming'] ?? 'Ara qıcqırma mərhələsi:'}} </b><br> {{$product->last_screaming}}</li>
                <li><b>{{$dictionary['fertilize_time'] ?? 'Mayalandırma müddəti:'}} </b><br> {{$product->fertilize_time}}</li>
            </ul>
        </div>
    </div>
    <div class="row pro_group1 margin_bottom_40">
        <div class="col-md-6 mo-1">
            <ul class="pro_block3">
                <li><b>{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}} </b> {{$product->storage_condition}}</li>
                <li> <b>{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}}</b> {{$product->storage_date}}</li>
            </ul>
        </div>
        <div class="col-md-3 mo-0">
            <div class="pro_img1"><img src="{{asset('storage/files/products/whole_wheat/whole_wheat_mix3.svg')}}"></div>
        </div>
        <div class="col-md-3 mo-2">
            <div class="pro_img1"><img src="{{asset('storage/files/products/whole_wheat/whole_wheat_mix4.svg')}}"></div>
        </div>
    </div>
</div>