<div class="container">
    <div class="pro_block12">
        <div class="left">
            @include('web.products.product_img',['product_name' => true])
        </div>
        <ul class="right">
            <li>
                <strong>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</strong>
                <p>{{$product->ingredients}}</p>

            </li>
            <li>
                <div class="b2">
                    <div>
                        <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
                        <div class="index">
                            {!! $product->use_amount !!}
                        </div>
                    </div>
                    <div>
                        <b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b><p>{{ $product->use_rule }}</p>
                    </div>
                </div>

            </li>
        </ul>
    </div>
    <div class="row pro_block2">
        <div class="col-md-3">
            <ul class="pro_block3">
                <li><b>{{$dictionary['temperature'] ?? 'Xəmirin temperaturu:'}} </b>{{$product->temperature}}</li>
                <li><b>{{$dictionary['cooking'] ?? 'Bişirmə:'}}</b><br>{{$product->cooking}}</li>
                <li><b>{{$dictionary['product_weight'] ?? 'Çəkisi:'}}</b><br>{{$product->weight}}</li>
            </ul>
        </div>
        <div class="col-md-3">
            <div class="pro_img1"><img src="{{asset('storage/files/products/mixed_grain/mixed_grain1.svg')}}"></div>
        </div>
        <div class="col-md-3">
            <ul class="pro_block3">
                <li><b>{{$dictionary['first_screaming'] ?? 'İlkin qıcqırma mərhələsi:'}} </b><br> {{$product->first_screaming}}</li>
                <li><b>{{$dictionary['last_screaming'] ?? 'Ara qıcqırma mərhələsi:'}} </b><br> {{$product->last_screaming}}</li>
                <li><b>{{$dictionary['fertilize_time'] ?? 'Mayalandırma müddəti:'}} </b><br> {{$product->fertilize_time}}</li>
            </ul>
        </div>
        <div class="col-md-3">
            <div class="pro_img1"><img src="{{asset('storage/files/products/mixed_grain/mixed_grain2.svg')}}"></div>
        </div>
    </div>
</div>
<div class="pro_block4 mixed_grain"  style="background: url({{asset('storage/files/products/mixed_grain/cover.png')}}) center no-repeat">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul>
                    <li>  <b>{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}} </b>{{$product->storage_condition}}</li>
                    <li><b>{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}}</b> {{$product->storage_date}}</li>
                </ul>
            </div>
        </div>
    </div>
</div>