<div class="container pandi_spagna_cocoa">
    <div class="row pro_group1 margin_top_10 margin_bottom">
        <div class="col-md-4">
            <div class="pro_block7">
                @include('web.products.product_img',['product_name' => true])
            </div>
        </div>
        <div class="col-md-4">
            <ul class="pro_block3">
                <li>

                    <b>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</b>
                    <p>{{$product->ingredients}}</p>

                </li>

                <li>

                    <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
                    <div class="index">
                        {!! $product->use_amount !!}
                    </div>

                </li>
            </ul>
        </div>
        <div class="col-md-4">
            <ul class="pro_block3">
                <li><b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b><br>{{ $product->use_rule }}</li>

                <li>
                    <b>{{$dictionary['temperature'] ?? 'Xəmirin temperaturu:'}}</b> {{$product->temperature}} <br>
                    <p>{{$product->cooking}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="row pro_group1">
        <div class="col-md-4 mo-1">
            <ul class="pro_block3">
                <li><b>{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}} </b><br> {{$product->storage_condition}}</li>
                <li><b>{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}}</b><br> {{$product->storage_date}}</li>
            </ul>
        </div>
        <div class="col-md-4 mo-0"><div class="pro_img1"><img src="{{asset('storage/files/products/pandi_spagna/pandi_spagna_cocoa1.svg')}}"></div></div>
        <div class="col-md-4 mo-2"><div class="pro_img1"><img src="{{asset('storage/files/products/pandi_spagna/pandi_spagna_cocoa2.svg')}}"></div></div>
    </div>
</div>