<div class="container">
    <div class="pro_block10 margin_top_10">
        <div class="left">
            <div class="header">

                <div>
                    @if(substr($product->getFirstMedia()->getFullUrl(), -3) == 'svg')
                        <img src="{{$product->getFirstMedia()->getFullUrl()}}"/>
                    @else
                        <img src="{{$product->getFirstMedia()->getUrl('blade')}}"/>
                    @endif

                    <span>{{$product->name}}</span>
                </div>

                <div>
                    <b>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</b>
                    <p>{{$product->ingredients}}</p>
                </div>
            </div>

            <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
            <div class="index">
                {!! $product->use_amount !!}
            </div>

            <b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b>
            <div><p>{{ $product->use_rule }}</p></div>

        </div>

        <div class="right">
            <img src="{{asset('storage/files/products/dark_rye/dark_rye_mix1.svg')}}"/>
        </div>

    </div>


    <div class="row pro_block2 margin_bottom">
        <div class="col-md-3 mo-0">

            <ul class="pro_block3 left25">
                <li><b>{{$dictionary['temperature'] ?? 'Xəmirin temperaturu:'}} </b>{{$product->temperature}}</li>
                <li><b class="block">{{$dictionary['cooking'] ?? 'Bişirmə:'}}</b> {{$product->cooking}}</li>
                <li><b class="block">{{$dictionary['product_weight'] ?? 'Çəkisi:'}}</b> {{$product->weight}}</li>
            </ul>
        </div>

        <div class="col-md-3 mo-2">
            <ul class="pro_block3 left25">

                <li><b  class="block">{{$dictionary['first_screaming'] ?? 'İlkin qıcqırma mərhələsi:'}} </b>{{$product->first_screaming}}</li>

                <li><b  class="block">{{$dictionary['last_screaming'] ?? 'Ara qıcqırma mərhələsi:'}} </b>{{$product->last_screaming}}</li>

                <li><b  class="block">{{$dictionary['fertilize_time'] ?? 'Mayalandırma müddəti:'}} </b>{{$product->fertilize_time}}</li>

            </ul>
        </div>
        <div class="col-md-3 mo-1">
            <div class="pro_img1"><img src="{{asset('storage/files/products/dark_rye/dark_rye_mix2.svg')}}"></div>
        </div>
        <div class="col-md-3 mo-3">
            <div class="pro_img1"><img src="{{asset('storage/files/products/dark_rye/dark_rye_mix3.svg')}}"></div>
        </div>
    </div>
    <div class="row pro_block2">
        <div class="col-md-3 mo-0">
            <div class="pro_img1"><img src="{{asset('storage/files/products/dark_rye/dark_rye_mix3.svg')}}"></div>
        </div>
        <div class="col-md-3 mo-2">
            <div class="pro_img1"><img src="{{asset('storage/files/products/dark_rye/dark_rye_mix4.svg')}}"></div>
        </div>
        <div class="col-md-6 mo-1">
            <ul class="pro_block3 left25">
                <li>
                    <b class="block">{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}} </b>{{$product->storage_condition}}
                </li>
                <li class="margin_top_20"><b class="block">{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}}</b>{{$product->storage_date}}</li>
            </ul>
        </div>
    </div>
</div>
