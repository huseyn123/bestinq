<div class="container muffin_vanilla_mix">
    <div class="row pro_group1 margin_top_10 margin_bottom">
        <div class="col-md-3">
            <div class="pro_block7">

               @include('web.products.product_img',['product_name' => true])

            </div>
        </div>
        <div class="col-md-9">
            <div class="pro_block8">
                <div class="b1">
                    <b>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</b>{{$product->ingredients}}
                </div>
                <div class="b2">
                    <div>
                        <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
                        <div class="index">
                            {!! $product->use_amount !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row pro_group1 muffin_vanilla_mix">
        <div class="col-md-6">
            <div class="row pro_block">
                <div class="col-md-6">
                    <div class="pro_img1"><img src="{{asset('storage/files/products/muffin_vanilla/muffin_vanilla_mix1.svg')}}"></div>
                </div>
                <div class="col-md-6">
                    <div class="pro_img1"><img src="{{asset('storage/files/products/muffin_vanilla/muffin_vanilla_mix2.svg')}}"></div>
                </div>
                <div class="col-md-6">
                    <div class="pro_img1"><img src="{{asset('storage/files/products/muffin_vanilla/muffin_vanilla_mix3.svg')}}"></div>
                </div>
                <div class="col-md-6">
                    <div class="pro_img1"><img src="{{asset('storage/files/products/muffin_vanilla/muffin_vanilla_mix4.svg')}}"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <ul class="pro_block3">

                <li><b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b><p>{{ $product->use_rule }}</p></li>
                <li><b>{{$dictionary['temperature'] ?? 'Xəmirin temperaturu:'}} {{$product->temperature}} </b> {{$product->cooking}}</li>
                <li><b>{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}} </b> {{$product->storage_condition}} </li>
                <li><b>{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}}</b> {{$product->storage_date}} </li>

            </ul>
        </div>
    </div>
</div>