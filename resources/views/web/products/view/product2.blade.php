<div class="container">
    <div class="row pro_group1 margin_top_10 margin_bottom">
        <div class="col-md-3">
            <div class="pro_block7">
                @include('web.products.product_img',['product_name' => true])
            </div>
        </div>
        <div class="col-md-9">
            <div class="pro_block8">
                <div class="b1">

                    <b>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</b>
                    <p>{{$product->ingredients}}</p>

                </div>
                <div class="b2">
                    <div>
                        <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
                        <div class="index">
                            {!! $product->use_amount !!}
                        </div>
                    </div>
                    <div>
                        <b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b>{{ $product->use_rule }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row pro_block2">
        <div class="col-md-3 mo-1">
            <ul class="pro_block3 right">
                <li><b>{{$dictionary['temperature'] ?? 'Xəmirin temperaturu:'}} </b>{{$product->temperature}}</li>
                <li><b>{{$dictionary['cooking'] ?? 'Bişirmə:'}}</b><br>{{$product->cooking}}</li>
                <li><b>{{$dictionary['product_weight'] ?? 'Çəkisi:'}}</b><br>{{$product->weight}}</li>



            </ul>
        </div>
        <div class="col-md-3 mo-0">
            <div class="pro_img1"><img src="{{asset('storage/files/products/cron_mix/cron_mix1.svg')}}"></div>
        </div>
        <div class="col-md-3 mo-2">
            <div class="pro_img1"><img src="{{asset('storage/files/products/cron_mix/cron_mix2.svg')}}"></div>
        </div>
        <div class="col-md-3 mo-3">
            <ul class="pro_block3">
                <li><b>{{$dictionary['first_screaming'] ?? 'İlkin qıcqırma mərhələsi:'}} </b><br> {{$product->first_screaming}}</li>
                <li><b>{{$dictionary['last_screaming'] ?? 'Ara qıcqırma mərhələsi:'}} </b><br> {{$product->last_screaming}}</li>
                <li><b>{{$dictionary['fertilize_time'] ?? 'Mayalandırma müddəti:'}} </b><br> {{$product->fertilize_time}}</li>

            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="pro_block9"  style="background: url({{asset('storage/files/products/cron_mix/cron_mix3.svg')}}) no-repeat">
                <b>{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}} </b>
                {{$product->storage_condition}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="pro_block9"  style="background: url({{asset('storage/files/products/cron_mix/cron_mix4.svg')}}) no-repeat">
                <b>{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}}</b>
                {{$product->storage_date}}
            </div>
        </div>
    </div>
</div>