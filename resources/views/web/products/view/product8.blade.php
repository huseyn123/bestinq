<div class="muffin_cocoa_mix">
    <div class="container">
        <div class="row pro_group2">
            <div class="col-md-6">
                <div class="pro_block14">
                    <div class="img">
                        @include('web.products.product_img')
                    </div>
                    <div class="text">
                        <h2 class="title">{{$product->name}}</h2>
                        <b>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</b><p>{{$product->ingredients}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="index">
                    <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
                    {!! $product->use_amount !!}
                </div>
            </div>
        </div>
        <div class="row pro_group1 margin_bottom">
            <div class="col-md-6 mo-1">
                <ul class="pro_block6">
                    <li><b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b><p>{{ $product->use_rule }}</p></li>

                    <li>
                        <b>{{$dictionary['temperature'] ?? 'Xəmirin temperaturu:'}} {{$product->temperature}}</b>
                        <p>{{$product->cooking}}</p>
                    </li>

                </ul>
            </div>
            <div class="col-md-3 mo-0">
                <div class="pro_img1"><img src="{{asset('storage/files/products/muffin_cocoa_mix/muffin_cocoa_mix1.svg')}}"></div>
            </div>
            <div class="col-md-3 mo-2">
                <div class="pro_img1"><img src="{{asset('storage/files/products/muffin_cocoa_mix/muffin_cocoa_mix2.svg')}}"></div>
            </div>
        </div>
        <div class="row pro_group1 margin_bottom">
            <div class="col-md-3 mo-0">
                <div class="pro_img1"><img src="{{asset('storage/files/products/muffin_cocoa_mix/muffin_cocoa_mix3.svg')}}"></div>
            </div>
            <div class="col-md-3 mo-2">
                <div class="pro_img1"><img src="{{asset('storage/files/products/muffin_cocoa_mix/muffin_cocoa_mix4.svg')}}"></div>
            </div>
            <div class="col-md-6 mo-1">
                <ul class="pro_block6">
                    <li><b>{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}} </b> {{$product->storage_condition}}</li>
                    <li><b>{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}}</b>{{$product->storage_date}}</li>
                </ul>
            </div>
        </div>
    </div>
</div>