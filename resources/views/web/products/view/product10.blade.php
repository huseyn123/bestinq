<div class="pro_block16">
    <div class="container">
        <div class="row pro_group1">
            <div class="col-md-3">
                <div class="pro_block7">
                    @include('web.products.product_img',['product_name' => true])
                </div>
            </div>
            <div class="col-md-3">
                <ul class="pro_block6">
                    <li>

                        <b>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</b>
                        <p>{{$product->ingredients}}</p>

                    </li>

                    <li>
                        <b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b>
                        <p> {{ $product->use_rule }}</p>
                    </li>

                </ul>
            </div>
            <div class="col-md-3">
                <div class="pro_img1">
                    <img src="{{asset('storage/files/products/sunflower_seeds/sunflower_seeds_mix1.svg')}}"/>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pro_img1">
                    <img src="{{asset('storage/files/products/sunflower_seeds/sunflower_seeds_mix2.svg')}}"/>
                </div>
            </div>
        </div>
        <div class="row pro_group1">
            <div class="col-md-6">
                <div class="pro_block3">
                    <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
                    <div class="index">
                        {!! $product->use_amount !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <ul class="pro_block3">
                    <li><b>{{$dictionary['temperature'] ?? 'Xəmirin temperaturu:'}} </b>{{$product->temperature}}</li>
                    <li><b>{{$dictionary['cooking'] ?? 'Bişirmə:'}}</b><br>{{$product->cooking}}</li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="pro_block3">
                    <li><b>{{$dictionary['first_screaming'] ?? 'İlkin qıcqırma mərhələsi:'}} </b><br> {{$product->first_screaming}}</li>
                    <li><b>{{$dictionary['fertilize_time'] ?? 'Mayalandırma müddəti:'}} </b><br> {{$product->fertilize_time}}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pro_block4 sunflower_seeds_mix"  style="background: url({{asset('storage/files/products/sunflower_seeds/cover.png')}}) no-repeat">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul>
                    <li><b>{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}}</b>  <p>{{$product->storage_condition}}</p> </li>
                    <li><b>{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}}</b> {{$product->storage_date}} </li>
                </ul>
            </div>
        </div>
    </div>
</div>