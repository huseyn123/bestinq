<div class="container">
    <div class="page_title2">{{$product->name}}</div>
    <div class="pro_block1">

        <div class="b1">
            <b>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</b>
            <p>{{$product->ingredients}}</p>

        </div>

        <div class="b2">
            <b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b>
            {{ $product->use_rule }}
        </div>

        <div class="b3">
            @include('web.products.product_img')
        </div>

        <div class="b4">
            <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
            {!! $product->use_amount !!}
        </div>

    </div>
    <div class="row pro_block2">
        <div class="col-md-3">
            <div class="pro_img1"><img src="{{asset('storage/files/products/bran_mix/bran_mix_img1.svg')}}"></div>
        </div>

        <div class="col-md-3">
            <ul class="pro_block3">
                <li><b>{{$dictionary['temperature'] ?? 'Xəmirin temperaturu:'}} </b>{{$product->temperature}}</li>
                <li><b>{{$dictionary['cooking'] ?? 'Bişirmə:'}}</b><br>{{$product->cooking}}</li>
                <li><b>{{$dictionary['product_weight'] ?? 'Çəkisi:'}}</b><br>{{$product->weight}}</li>
            </ul>
        </div>

        <div class="col-md-3">
            <div class="pro_img1"><img src="{{asset('storage/files/products/bran_mix/bran_mix_img2.svg')}}"></div>
        </div>

        <div class="col-md-3">
            <ul class="pro_block3">
                <li><b>{{$dictionary['first_screaming'] ?? 'İlkin qıcqırma mərhələsi:'}} </b><br> {{$product->first_screaming}}</li>
                <li><b>{{$dictionary['last_screaming'] ?? 'Ara qıcqırma mərhələsi:'}} </b><br> {{$product->last_screaming}}</li>
                <li><b>{{$dictionary['fertilize_time'] ?? 'Mayalandırma müddəti:'}} </b><br> {{$product->fertilize_time}}</li>
            </ul>
        </div>
    </div>

</div>
<div class="pro_block4" style="background: url({{asset('storage/files/products/bran_mix/cover.svg')}}) center no-repeat">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul>
                    <li><b>{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}} </b>{{$product->storage_condition}}</li>
                    <li><b>{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}} </b>{{$product->storage_date}}</li>
                </ul>
            </div>
        </div>
    </div>
</div>