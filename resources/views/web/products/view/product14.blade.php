<div class="moisty_cake_bg">
    <div class="container">
        <div class="pro_block12 moisty_cake">
            <div class="left">
                @include('web.products.product_img',['product_name' => true])
            </div>
            <ul class="right">
                <li>
                    <strong>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</strong>
                    <p>{{$product->ingredients}}</p>
                </li>
                <li>
                    <div class="b2">
                        <div>
                            <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
                            <div class="index">
                                {!! $product->use_amount !!}
                            </div>

                        </div>
                        <div>
                            <b>{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}}</b>
                            <p>{{$product->storage_date}}</p>
                        </div>
                    </div>

                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="pro_block13">
                    <div class="row">
                        <div class="col-md-6 col-xs-5">
                            <div class="img"><img src="{{asset('storage/files/products/moisty_cake/moisty_cake1.svg')}}" /></div>
                        </div>
                        <div class="col-md-6 col-xs-7">
                            <div class="text">
                                <b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b>
                                <p>{{ $product->use_rule }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pro_block13">
                    <div class="row">
                        <div class="col-md-6 col-xs-5">
                            <div class="img"><img src="{{asset('storage/files/products/moisty_cake/moisty_cake2.svg')}}" /></div>
                        </div>
                        <div class="col-md-6 col-xs-7">
                            <div class="text">
                                <b>{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}}</b>
                                {{$product->storage_condition}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>