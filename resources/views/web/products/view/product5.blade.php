<div class="container waffle_mix">
    <div class="row pro_group1 margin_top_20 margin_bottom_40">
        <div class="col-md-3">
            <div class="pro_block7">

                @include('web.products.product_img',['product_name' => true])

            </div>
        </div>
        <div class="col-md-9">
            <div class="pro_block8">
                <div class="b1">
                    <b>{{$dictionary['ingredient'] ?? 'Tərkibi!:'}}</b>
                    <p>{{$product->ingredients}}</p>
                </div>
                <div class="b2">
                    <div>
                        <b>{{$dictionary['use_amount'] ?? 'İstifadə miqdarı:!'}}</b>
                        <div class="index">
                            {!! $product->use_amount !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pro_block4 waffle_mix margin_top_10" style="background: url({{asset('storage/files/products/waffle_mix/cover.png')}})">
    <div class="container">
        <div class="row pro_group1">
            <div class="col-md-6">
                <ul class="pro_ul">
                    <li><b>{{$dictionary['use_rule'] ?? 'İstifadə qaydasi:!'}}</b><p>{{ $product->use_rule }}</p></li>
                    <li><b>{{$dictionary['temperature'] ?? 'Xəmirin temperaturu:'}} </b>{{$product->temperature}}</li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="pro_ul">
                    <li>
                        <b>{{$dictionary['storage_condition'] ?? 'Saxlama şəraiti:'}} </b>
                        <p>{{$product->storage_condition}}</p>
                    </li>

                    <li>
                        <b>{{$dictionary['storage_date'] ?? 'Saxlama müddəti:'}}</b>
                        <p>{{$product->storage_date}}</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>