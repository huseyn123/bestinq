@if($product->getFirstMedia())

    @if(substr($product->getFirstMedia()->getFullUrl(), -3) == 'svg')
        <img src="{{$product->getFirstMedia()->getFullUrl()}}"/>
    @else
        <img src="{{$product->getFirstMedia()->getUrl('blade')}}"/>
    @endif

    {{ isset($product_name) ? $product->name : ''}}

@endif

