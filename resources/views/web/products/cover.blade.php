<div class="page_header">

    @if ($product->getFirstMedia('cover'))
        <img src="{{$product->getFirstMedia('cover')->getUrl('blade')}}"/>
    @else
        <img src="{{asset('images/product_header_img.png')}}"/>
    @endif

    <div>{{$product->name}}</div>

</div>