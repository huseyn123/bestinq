@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>

    <div class="cert container">
        <h1 class="page_title">{{$page->seo_title}}</h1>
        <div class="row">

            @foreach($certificates as $certificate)
                <div class="col-md-3 col-xs-6">
                    @if($certificate->getFirstMedia())
                        <a class="item" data-fancybox="gallery"
                           href="@if(substr($certificate->getFirstMedia()->getFullUrl(), -3) == 'svg') {{asset($certificate->getFirstMedia()->getFullUrl())}} @else{{ asset($certificate->getFirstMedia()->getUrl('blade')) }}@endif"
                           data-caption="Sertifikat {{$loop->iteration}}">

                            <span class="img"><img src="@if(substr($certificate->getFirstMedia()->getFullUrl(), -3) == 'svg') {{asset($certificate->getFirstMedia()->getFullUrl())}} @else {{ asset($certificate->getFirstMedia()->getUrl('thumb')) }}@endif"/></span>
                            <span class="title">{{ $certificate->name }}</span>
                            @if($certificate->no)<span class="desc">No: {{ $certificate->no }}</span>@endif
                        </a>
                     @endif

                </div>
            @endforeach

        </div>


        <!-- Pagination Begin -->
            {!! $certificates->appends(request()->input())->links('vendor.pagination.news') !!}
        <!-- Pagination End -->


    </div>


@endsection