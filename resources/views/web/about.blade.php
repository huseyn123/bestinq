@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.cover')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>

    <div class="container about_company">
        <div class="row pro_group1">
            <div class="col-md-6">
                <div class="pro_block11">
                    <h1 class="page_title">{{$page->seo_title}}</h1>
                    {!! $page->content !!}
                </div>
            </div>

            <div class="col-md-6">
                @if($page->getFirstMedia())
                    <div class="pro_img1"><img src="{{$page->getFirstMedia()->getUrl('blade')}}"/></div>
                @endif
            </div>

        </div>
    </div>

    <div class="pro_block15 about_company" style="background: url({{asset('storage/files/sirket_haqqinda/cover.svg')}})">
        <div class="container">
            <div class="item" style="background: url({{asset('storage/files/sirket_haqqinda/product1.svg')}}) center 23px no-repeat"><b>{{$config['product1_count']}}</b>{{$dictionary['about_product1'] ?? 'Məhsul'}}</div>
            <div class="item" style="background: url({{asset('storage/files/sirket_haqqinda/product2.svg')}}) center 35px no-repeat"><b>{{$config['product2_count']}}</b>{{$dictionary['about_product2'] ?? 'Çörək'}}</div>
            <div class="item" style="background: url({{asset('storage/files/sirket_haqqinda/product3.svg')}}) center 23px no-repeat"><b>{{$config['product3_count']}}</b>{{$dictionary['about_product3'] ?? 'Kilo un'}}</div>
        </div>
    </div>

    <div class="container about_company">

        @foreach($blocks as $block)

            <div class="row pro_group1">
                <div class="col-md-6">
                    @if($block->getFirstMedia())
                        <div class="pro_img1"><img src="{{$block->getFirstMedia()->getUrl('blade')}}"/></div>
                    @endif
                </div>
                <div class="col-md-6">
                    <div class="pro_block11">
                        <h2 class="page_title">{{$block->name}}</h2>
                        {!! $block->content !!}
                    </div>
                </div>
            </div>

        @endforeach
    </div>


@endsection
