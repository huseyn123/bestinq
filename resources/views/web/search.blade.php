@extends ('layouts.web', ['page_heading' => $dictionary['search_page'] ?? 'Axtarışın nəticələri'])

@section ('content')

<div class="container">

	<ol id="breadcrumb" class="container" itemscope itemtype="http://schema.org/BreadcrumbList">
		<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="{{ route('home') }}"><span itemprop="name">Ana səhifə</span></a>
			<span itemprop="position" content="1"><i class="fa fa-angle-right"></i></span>
		</li>
		<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
			<a itemprop="item" ><span itemprop="name">Axtarışın Nəticəsi</span></a>
			<span itemprop="position" content="2"></span>
		</li>
	</ol>
	<!-- BreadCrumbs End -->
</div>



<div class="container search_page">

	<div class="keyword">"Besting"</div>
	<div class="result_count">{{$pages->total()}}  nəticə tapıldı</div>

	@foreach($pages as $page)

		<div class="item">
			<span class="title">{{$page->name}}</span>
			@if($page->summary) {{str_limit( $page->summary, 200) }} @else {!! str_limit($page->content, 200) !!} @endif

			<a href="{{route('showPage',$page->slug)}}">Ətraflı<i class="fa fa-angle-right"></i></a>
		</div>

	@endforeach


	<!-- Pagination Begin -->
		{!! $pages->appends(request()->input())->links('vendor.pagination.news') !!}
	<!-- Pagination End -->

</div>



@endsection



