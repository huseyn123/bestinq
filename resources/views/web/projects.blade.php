@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.breadcrumbs')


    <!-- ProjectWrapper Begin -->
    <section class="project_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>{{ $page->name }}</h1>
                    @if($years->count())
                        <div class="project_years">
                            <ul class="list-unstyled">
                                @foreach($years as $year)
                                    <li  @if($selectedYear == $year) class="active" @endif><a href="{{ route('showPage',[$page->slug]).'?year='.$year }}"  >{{ $year }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="project_list">
                        <div class="row">
                            @include('web.elements.productList')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ProjectWrapper End -->



@endsection

@push('scripts')
    @include('web.elements.other_js')
@endpush



