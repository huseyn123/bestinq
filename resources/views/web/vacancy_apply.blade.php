@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')


    @include('web.elements.breadcrumbs')

    <div class="for_media container">
        <div class="row ">
            <div class="col-md-8 col-md-offset-2">
                <div class="form">
                    @include('web.form.form_succes')
                    <h3 class="title text-left"></h3>

                    @include('web.form.vacancy_apply')

                </div>
            </div>
        </div>
    </div>


@endsection
