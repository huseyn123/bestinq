@extends ('layouts.user-cabinet')

@section ('section')


    <nav class="account_tab clearfix">
        <a href="{{ route('user_app.all') }}" {{ activeUrl(route('user_app.all')) }}>Edilən yardımlar</a>
        <a href="{{ route('user_app.statistics') }}" {{ activeUrl(route('user_app.statistics')) }}>Statistika</a>
    </nav>

    <div class="product_list row">

        @foreach($applications as $ap)

            <div class="col-md-4 col-sm-3 col-xs-6 col-mob-12">
                <div class="box">

                    @if($ap->getFirstMedia())
                        <figure>
                            <img src="{{ asset($ap->getFirstMedia()->getUrl('blade')) }}" alt="{{$ap->name}}">
                        </figure>
                    @endif

                    <div class="body">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ percentage($ap->amount,$ap->done_amount) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ percentage($ap->amount,$ap->done_amount) }}%"></div>
                            <span>{{ percentage($ap->amount,$ap->done_amount) }}%</span>
                        </div>
                        <span class="price">{{ $ap->related_with_date->sum('application_price') }}  <span>AZN</span></span>
                        <h2>{{ $ap->name }}</h2>
                        <ul class="list-unstyled">
                            <li><span>Kateqoriya:</span>{{ $ap->page_name }}</li>
                            <li><span>Məbləğ:</span>{{ $ap->amount }} AZN</li>
                            <li><span>Son tarix:</span>{{ blogDate($ap->end_date) }}</li>
                        </ul>
                        <span class="more"><a href="{{ route('showPage',[$ap->page_slug,$ap->slug]) }}" title="">Ətraflı bax</a></span>
                    </div>
                </div>
            </div>

        @endforeach


    </div>


@endsection



