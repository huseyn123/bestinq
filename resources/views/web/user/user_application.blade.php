@extends ('layouts.user-cabinet')

@section ('section')

    <div class="account_form">
        <div class="row">
            {!! Form::open(['url'=>route('user_app.post',[$application->id]), 'method'=>'POST', 'id' => 'user_app_form']) !!}
                <div class="col-xs-6 col-mob-12">
                    <div class="ipt_group">
                        {!! Form::text('name', null, ["placeholder" => '*Ad daxil edin','class' => 'ipt_style ']) !!}
                    </div>
                </div>
                <div class="col-xs-6 col-mob-12">
                    <div class="ipt_group">
                        {!! Form::text('surname', null, ["placeholder" => '*Soyad daxil edin','class' => 'ipt_style ']) !!}
                    </div>
                </div>
                <div class="col-xs-6 col-mob-12">
                    <div class="ipt_group">
                        {!! Form::text('price', null, ["placeholder" => '*Məbləğ','class' => 'ipt_style','onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57']) !!}
                        <span class="azn">AZN</span>
                    </div>
                </div>
                <div class="col-xs-12">
                    {{--<div class="checkbox">--}}
                        {{--<input type="checkbox" name="account_form" id="account_form" class="css-checkbox">--}}
                        {{--<label for="account_form" class="css-label">Şərtlərlə razıyam</label>--}}
                    {{--</div>--}}
                    {!! Form::button($dictionary['send'], ['id' => 'loadingButton', 'data-loading-text' => loading(), 'autocomplete' => 'off', 'type' => 'submit']) !!}
                    <p id="msg"></p>
                </div>
            {!! Form::close() !!}

        </div>
    </div>

@endsection