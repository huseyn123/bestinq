@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumbs')

    <!-- Wrapper Begin -->
    <section id="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PageCover Begin -->
                    @if($page->getFirstMedia())
                        <div class="page_cover" style="background-image: url({{ asset($page->getFirstMedia()->getUrl('cover')) }});"></div>
                    @endif
                    <!-- PageCover End -->
                    <!-- StaticPage Begin -->
                    <div class="static_page">
                        <!-- PageTitle -->
                        <div class="page_title">
                            <h1>{{ $page->name }}</h1>
                        </div>
                        <!-- Text -->
                        <div class="text">
                            {!!  $page->content !!}
                        </div>

                        @if($page->children->count())
                                <div class="company_values">
                                    <div class="row">
                                        @foreach($page->children as $child)
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="box">
                                                    <h2>{{ $child->name }}</h2>
                                                    {!! $child->content !!}
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                         @endif
                    </div>

                    <!-- StaticPage End -->
                </div>
            </div>
        </div>
    </section>
    <!-- Wrapper End -->

@endsection
@push('scripts')
<script>
    $('.static_page ul').addClass('ks_list list-unstyled clearfix');
    $( ".static_page ul li" ).prepend( '<i class="fa fa-check" aria-hidden="true">' );

</script>
@endpush