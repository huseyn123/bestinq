@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>

    <div class="container our_mission">
        <h1 class="page_title">{{$page->seo_title}}</h1>
        <div class="row">
            <div class="col-md-4">
                @if($page->getFirstMedia())
                    <div class="pro_img2"><img src="{{$page->getFirstMedia()->getUrl('blade')}}"/></div>
                @endif
            </div>
            <div class="col-md-8">
                <div class="pro_block11">
                    <h2 class="page_title">{{$page->name}}</h2>
                    {!! $page->content !!}
                </div>
            </div>
        </div>
    </div>

@endsection