@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>


    <div class="container vacancy">
        <h1 class="page_title">{{$page->seo_title}}</h1>

        <div class="row">

            @foreach($vacancies as $vacancy)

                <div class="col-md-6">
                    <div class="item">
                        <h3 class="title">{{ $vacancy->position }}</h3>
                        <div class="body">
                            <div class="text">

                                {!! $vacancy->work_information !!}

                                <p>Qeyd olunmuş tələblərə cavab verdiyiniz halda CV-nizi (anketlərinizi)  <a>cv@besting.az</a> elektron ünvanına Subject/Mövzu hissəsində vəzifəni qeyd etməklə göndərə bilərsiniz.</p>

                            </div>
                        </div>
                        <a class="toggle"><i class="fa fa-angle-down"></i></a>
                    </div>
                </div>

            @endforeach

        </div>
    </div>


@endsection
