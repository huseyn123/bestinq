@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="blog_page blog_list">

        <div class="container">
            @include('web.elements.breadcrumbs')
        </div>


        <div class="container">
            <h1 class="page_title">{{$page->seo_title}}</h1>
            <div class="row" data-match-height=".item">

                @include('web.elements.news-grid',['data' =>$articles])

            </div>
        </div>

        <!-- Pagination Begin -->
            {!! $articles->appends(request()->input())->links('vendor.pagination.news') !!}
        <!-- Pagination End -->


    </div>


@endsection
