@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>


    <div class="container satisfaction_survey">
        <h1 class="page_title">{{$page->seo_title}}</h1>
        <div class="desc">{{$dictionary['stf_text'] ?? 'Bu sorğuda iştirak etməklə, sizin üçün daha yaxşı xidmət göstərə bilməyimizə yardımçı olursunuz.'}}</div>
        <div class="form main_st_id">
            @include('web.elements.succes_form')

            @include('web.form.satisfaction',['id'=>'st_id','page_name'=>$page->name])
        </div>
    </div>

@endsection
