@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.cover')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>

    <div class="container quality_policy">
        <h1 class="page_title">{{$page->seo_title}}</h1>
        <div class="row" data-match-height=".item">

            @foreach($blocks as $block)

                <div class="col-md-6">
                    <div class="item">
                        {!! $block->content !!}
                    </div>
                </div>

            @endforeach

        </div>
    </div>

@endsection
