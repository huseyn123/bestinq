<div class="row" data-match-height=".item">

    @foreach($kivs as $kiv)

        <div class="col-md-3 col-xs-6">
            <div class="item">
                @if($kiv->getFirstMedia('logo_image'))
                    <div class="img">
                        <img src="{{ asset($kiv->getFirstMedia('logo_image')->getFullUrl()) }}"/>
                    </div>
                @endif

                <ul>
                    @if($kiv->getFirstMedia('eps_file'))<li><a href="{{asset($kiv->getFirstMedia('eps_file')->getFullUrl())}}" download>EPS</a></li>@endif
                    @if($kiv->getFirstMedia('png_file'))<li><a href="{{asset($kiv->getFirstMedia('png_file')->getFullUrl())}}" download>PNG</a></li>@endif
                </ul>
            </div>
        </div>

    @endforeach

</div>

<div class="row" data-match-height=".item">

</div>