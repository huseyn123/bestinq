<div class="row" data-match-height=".item">

    @foreach($kivs as $kiv)

        <div class="col-md-3 col-xs-6">
            <a class="item"  @if($kiv->youtube_id)data-fancybox href="{{$kiv->youtube_id}}" @endif>
                @if($kiv->getFirstMedia())
                    <span class="img">
                        <img src="{{$kiv->getFirstMedia()->getUrl('blade')}}">
                    </span>
                @endif

                <span class="title">{{$kiv->title}}</span>
            </a>
        </div>

    @endforeach
</div>