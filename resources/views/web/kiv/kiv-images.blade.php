<div class="row" data-match-height=".item">

    @foreach($page->getMedia('gallery') as $gallery)

        <div class="col-md-3 col-xs-6">
            <div class="item">
                <img src="{{$gallery->getUrl('blade')}}">
                <a href="{{$gallery->getUrl('blade')}}" download>{{$dixtionary['download'] ?? 'Yüklə'}}</a>
            </div>
        </div>

    @endforeach

</div>
