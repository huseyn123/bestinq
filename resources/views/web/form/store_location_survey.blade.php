{!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => 'store_location_survey','files' => true]) !!}
<div class="row">
        <div class="col-md-4">{!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'required' => 'required']) !!}</div>
        <div class="col-md-4">{!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'class' => 'phonenumber','required' => 'required']) !!}</div>
        <div class="col-md-4">{!! Form::email('email', null, ["placeholder" => '*'.$dictionary['email'],'required' => 'required']) !!}</div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::textarea('address', null, ["placeholder" => $dictionary['store_location_address'] ?? '* Təklif etdiyiniz yerin ünvanı','required' => 'required']) !!}
    </div>
    </div>
    <div class="row">
        <div class="col-md-4">{!! Form::text('area', null, ["placeholder" => $dictionary['store_location_area_text'] ?? '* Yerin sahəsi m','required' => 'required']) !!}</div>
        <div class="col-md-4">
            <span class="radio_label">{{$dictionary['depot'] ?? '* Anbar sahəsi'}}</span>
            <label class="radio"><input type="radio" name="depot" value="{{ $dictionary['have'] }}" required><span>{{ $dictionary['have'] }}</span></label>
            <label class="radio"><input type="radio" name="depot" value="{{ $dictionary["have't"] }}" required><span>{{ $dictionary["have't"] }}</span></label>
        </div>
        <div class="col-md-4">
            <span class="radio_label">{{$dictionary['parking'] ?? '* Avtopark'}}</span>
            <label class="radio"><input type="radio" name="parking" value="{{ $dictionary['have'] }}" required><span>{{ $dictionary['have'] }}</span></label>
            <label class="radio"><input type="radio" name="parking" value="{{ $dictionary["have't"] }}" required><span>{{ $dictionary["have't"] }}</span></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::textarea('message', null, ["placeholder" => $dictionary['disclosure'] ?? '* Açıqlama','rows' => "9",'required' => 'required']) !!}
            {{ Form::hidden('method', 'store_location_surver') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="file_input">
                {!! Form::text(null, null, ['required' => 'required']) !!}
                <span class="button">{{$dictionary['insert_foto'] ?? '* Şəkil əlavə et'}}</span>
                {!! Form::file('image',['required' => 'required']) !!}

            </div>
            <div class="helper">{{$dictionary['image_format'] ?? 'Şəkilləri zip,pdf,png kimi əlavə edə bilərsiz'}}</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            @include('web.elements.error-message')
            {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
        </div>
    </div>
{!! Form::close() !!}