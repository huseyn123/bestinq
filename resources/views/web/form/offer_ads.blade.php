{!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => 'offer_ads']) !!}

    <div class="row">
        <div class="col-md-4">{!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'required' => 'required']) !!}</div>
        <div class="col-md-4">{!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'class' => 'phonenumber','required' => 'required']) !!}</div>
        <div class="col-md-4">{!! Form::email('email', null, ["placeholder" => '*'.$dictionary['email'],'required' => 'required']) !!}</div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {!! Form::select('shop',$shops->pluck('name','name')->prepend($dictionary['all_shops'],$dictionary['all_shops']),['required'=>'required']) !!}
        </div>
        <div class="col-md-6">
            <select name="ads_type" required>
                <option value>{{$dictionary['select_of_a_type'] ?? '* Reklamın növünü seçin'}}</option>
                @foreach(config('config.ads_form_select') as $key=>$value)
                    <option value="{{$dictionary[$key] ?? $value}}">{{$dictionary[$key] ?? $value}}</option>
                @endforeach

            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::textarea('message', null, ["placeholder" => $dictionary['y_offer'] ?? 'Təklifiniz','required' => 'required']) !!}
            {{ Form::hidden('method', 'offer_ads') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
        </div>
    </div>
