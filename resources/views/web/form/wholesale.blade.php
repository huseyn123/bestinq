{{--<form method="post" class="validation" action="/elaqe_success.html">--}}
    {{--<div class="row">--}}
        {{--<div class="col-xs-12"><input name="name" class="validate[required]" type="text" placeholder="Ad, Soyad" /></div>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
        {{--<div class="col-xs-12"><input name="phone" class="validate[required]" type="text" placeholder="Telefon nömrəsi" /></div>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
        {{--<div class="col-xs-12"><input name="email" class="validate[required,custom[email]]" type="email" placeholder="E-mail" /></div>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
        {{--<div class="col-xs-12"><input name="subject" class="validate[required]" type="text" placeholder="Mövzu" /></div>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
        {{--<div class="col-xs-12">--}}
            {{--<label class="radio"><input name="t" type="radio" value="1"><span>Tərəfdaş ol</span></label>--}}
            {{--<label class="radio"><input name="t" type="radio" value="2"><span>Topdan satış</span></label>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
        {{--<div class="col-xs-12"><textarea class="validate[required]" placeholder="Mesajınız..."></textarea></div>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
        {{--<div class="col-xs-12 text-right">--}}
            {{--<button type="submit">GÖNDƏR</button>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</form>--}}

{!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => $id,'class' => "validation main_f"]) !!}
<div class="row">
    <div class="col-xs-12">
        {!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'required' => 'required','class' => "validate[required]"]) !!}
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        {!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'required' => 'required','class' => "validate[required] phonenumber"]) !!}
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        {!! Form::email('email', null, ["placeholder" => '*'.$dictionary['email'],'required' => 'required','class' => "validate[required]"]) !!}
    </div>
</div>
    <div class="row">
        <div class="col-xs-12">
            {{ Form::hidden('method', 'wholesale') }}
            {!! Form::text('subject', null, ["placeholder" => '*'.$dictionary['subject'],'required' => 'required','class' => "validate[required]"]) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <label class="radio"><input name="wr" class="validate[required]" type="radio"  required="required" value="{{$dictionary['wr_1'] ?? 'Tərəfdaş ol'}}"><span>{{$dictionary['wr_1'] ?? 'Tərəfdaş ol'}}</span></label>
            <label class="radio"><input name="wr" class="validate[required]" type="radio" required="required" value="{{$dictionary['wr_2'] ?? 'Topdan satış'}}"><span>{{$dictionary['wr_2'] ?? 'Topdan satış'}}</span></label>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            {!! Form::textarea('message', null, ['class' => 'validate[required]"',"placeholder" => $dictionary['message'],'required' => 'required']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-right">
            {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
        </div>
    </div>
{!! Form::close() !!}
