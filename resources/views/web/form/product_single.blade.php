<div class="form2container">
    <div class="form main_product_form">

        @include('web.elements.succes_form')


        <div class="title">{{$dictionary['form_title'] ?? 'Müraciət formu'}}</div>
        {!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => 'product_form','class' => "validation"]) !!}

            <div class="row">

                <div class="col-md-6">
                    {!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'required' => 'required','class' => "validate[required]"]) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'required' => 'required','class' => "validate[required] phonenumber"]) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {!! Form::email('email', null, ["placeholder" => '*'.$dictionary['email'],'required' => 'required','class' => "validate[required]"]) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('subject', null, ["placeholder" => '*'.$dictionary['subject'],'required' => 'required','class' => "validate[required]"]) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    {!! Form::textarea('message', null, ["placeholder" => $dictionary['message'],'required' => 'required','class' => "validate[required] "]) !!}
                    {{ Form::hidden('method', 'product') }}
                    {{ Form::hidden('p_name',$p_name) }}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-right">
                    @include('web.elements.error-message')
                    {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>