{!! Form::open(['url'=>route('product.search'), 'method'=>'GET']) !!}

    <div class="filter">
        <div>
            {!! Form::select('shop_id',$shops->pluck('name','id')->prepend($dictionary['all_shops'],''),request()->get('shop_id'),["id" => "shop_id"]) !!}
        </div>
        <div>
            <select name="branch_id" id="branch_id">
                <option value>{{$dictionary['all_brands']}}</option>
                @foreach($brands as $brand)
                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                @endforeach
            </select>
        </div>
        <div>
            {!! Form::select('category',$category->prepend($dictionary['select_category'],''),request()->get('category')) !!}

        </div>
        <div>
            <div class="search">
                {!! Form::text('keyword', null, ["placeholder" => $dictionary['write_product'],'autocomplete' => 'off']) !!}
            </div>
        </div>
    </div>
    {!! Form::button(null, ['style' =>"display: none",'type' => 'submit']) !!}

{!! Form::close() !!}