{!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => $id,'class' => "validation main_f"]) !!}

    <div class="row">
        <div class="col-md-6">
            {!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'required' => 'required','class' => "validate[required]"]) !!}

        </div>
        <div class="col-md-6">
            {!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'required' => 'required','class' => "validate[required] phonenumber"]) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {!! Form::email('email', null, ["placeholder" => '*'.$dictionary['email'],'required' => 'required','class' => "validate[required]"]) !!}
            {{ Form::hidden('method', 'contact') }}

        </div>
        <div class="col-md-6">
            {!! Form::text('subject', null, ["placeholder" => '*'.$dictionary['subject'],'required' => 'required','class' => "validate[required]"]) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="radio"><input name="w1" type="radio" value="{{$dictionary['con_t1'] ?? 'İrad'}}"><span>{{$dictionary['con_t1'] ?? 'İrad'}} </span></label>
            <label class="radio"><input name="w1" type="radio" value="{{$dictionary['con_t2'] ?? 'Təklif'}}"><span>{{$dictionary['con_t2'] ?? 'Təklif'}}</span></label>
        </div>
        <div class="col-md-6">
            <label class="radio"><input name="w2" type="radio" value="{{$dictionary['con_t3'] ?? 'Müştərilərə'}}"><span>{{$dictionary['con_t3'] ?? 'Müştərilərə'}}</span></label>
            <label class="radio"><input name="w2" type="radio" value="{{$dictionary['con_t4'] ?? 'Marketlərə'}}"><span>{{$dictionary['con_t4'] ?? 'Marketlərə'}}</span></label>
        </div>
    </div>
    <div class="row">

        <div class="col-xs-12">
            {!! Form::textarea('message', null, ['class' => 'validate[required]"',"placeholder" => $dictionary['message'],'required' => 'required']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-right">
            {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
        </div>
    </div>

{!! Form::close() !!}
