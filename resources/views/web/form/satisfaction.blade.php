    {!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => $id,'class' => "validation main_f"]) !!}

    <div class="row">
        <div class="col-md-4">
            {!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'required' => 'required','class' => "validate[required]"]) !!}

        </div>
        <div class="col-md-4">
            {!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'required' => 'required','class' => "validate[required] phonenumber"]) !!}
        </div>
        <div class="col-md-4">
            {!! Form::email('email', null, ["placeholder" => '*'.$dictionary['email'],'required' => 'required','class' => "validate[required]"]) !!}
        </div>
        {{ Form::hidden('method', 'satification') }}
    </div>
    <hr>
    <div class="row radio_row">
        <div class="col-md-4"><span class="radio_label">{{$dictionary['st_title'] ?? '1. Qiymətlər sizin fikrinizcə necədir?'}}</span></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-3"><label class="radio"><input type="radio" name="st1" value="{{$dictionary['st_check1'] ?? 'Ucuzdur'}}" required><span>{{$dictionary['st_check1'] ?? 'Ucuzdur'}}</span></label></div>
                <div class="col-md-3"><label class="radio"><input type="radio" name="st1" value="{{$dictionary['st_check2'] ?? 'Bahadır'}}" required><span>{{$dictionary['st_check2'] ?? 'Bahadır'}}</span></label></div>
                <div class="col-md-3"><label class="radio"><input type="radio" name="st1" value="{{$dictionary['st_check3'] ?? 'Çox bahadır'}}" required><span>{{$dictionary['st_check3'] ?? 'Çox bahadır'}}</span></label></div>
                <div class="col-md-3"><label class="radio"><input type="radio" name="st1" value="{{$dictionary['st_check4'] ?? 'Müqayisə etməmişəm'}}" required><span>{{$dictionary['st_check4'] ?? 'Müqayisə etməmişəm'}}</span></label></div>
            </div>
        </div>
    </div>
    <div class="row radio_row">
        <div class="col-md-4"><span class="radio_label">{{$dictionary['st_title2'] ?? '2. Məhsul çeşidlərindən razısınızmı?'}}</span></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-3"><label class="radio"><input type="radio" name="st2" value="{{$dictionary['st_check5'] ?? 'Razıyam'}}" required><span>{{$dictionary['st_check5'] ?? 'Razıyam'}}</span></label></div>
                <div class="col-md-3"><label class="radio"><input type="radio" name="st2" value="{{$dictionary['st_check6'] ?? 'Diqqətimi çəkmir'}}" required><span>{{$dictionary['st_check6'] ?? 'Diqqətimi çəkmir'}}</span></label></div>
                <div class="col-md-3"><label class="radio"><input type="radio" name="st2" value="{{$dictionary['st_check7'] ?? 'Razı deyiləm'}}" required><span>{{$dictionary['st_check7'] ?? 'Razı deyiləm'}}"</span></label></div>
                <div class="col-md-3"><label class="radio"><input type="radio" name="st2" value="{{$dictionary['st_check8'] ?? 'Heç razı deyiləm'}}" required><span>{{$dictionary['st_check8'] ?? 'Heç razı deyiləm'}}</span></label></div>
            </div>
        </div>
    </div>
    <div class="row radio_row">
        <div class="col-md-4"><span class="radio_label">{{$dictionary['st_title3'] ?? '3. Məhsul keyfiyyəti və təzəliyi necədir?'}}</span></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-3"><label class="radio"><input type="radio" name="st3" value="{{$dictionary['st_check9'] ?? 'Çox yaxşıdır'}}" required><span>{{$dictionary['st_check9'] ?? 'Çox yaxşıdır'}}</span></label></div>
                <div class="col-md-3"><label class="radio"><input type="radio" name="st3" value="{{$dictionary['st_check10'] ?? 'Yaxşıdır'}}" required><span>{{$dictionary['st_check10'] ?? 'Yaxşıdır'}}</span></label></div>
                <div class="col-md-3"><label class="radio"><input type="radio" name="st3" value="{{$dictionary['st_check11'] ?? 'Pisdir'}}" required><span>{{$dictionary['st_check11'] ?? 'Pisdir'}}</span></label></div>
                <div class="col-md-3"><label class="radio"><input type="radio" name="st3" value="{{$dictionary['st_check12'] ?? 'Çox pisdir'}}" required><span>{{$dictionary['st_check12'] ?? 'Çox pisdir'}}</span></label></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-right">
            {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
        </div>
    </div>
{!! Form::close() !!}
