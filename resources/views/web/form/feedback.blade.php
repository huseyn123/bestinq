{!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => 'feedback_form']) !!}
<div class="row">
    <div class="col-md-6">{!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'required' => 'required']) !!}</div>
    <div class="col-md-6">{!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'class' => 'phonenumber','required' => 'required']) !!}</div>
</div>
<div class="row">
    <div class="col-md-6"> {!! Form::email('email', null, ["placeholder" => '*'.$dictionary['email'],'required' => 'required']) !!}</div>
    <div class="col-md-6">
        {!! Form::select('shop',$shops->pluck('name','name')->prepend($dictionary['all_shops'],$dictionary['all_shops']),['required'=>'required']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label class="radio"><input type="radio" name="f_type" value="{{$dictionary['f_type1_text'] ?? 'İrad '}}" required><span>{{$dictionary['f_type1_text'] ?? 'İrad '}}</span></label>
        <label class="radio"><input type="radio" name="f_type" value="{{$dictionary['f_type2_text'] ?? 'Təklif '}}" required><span>{{$dictionary['f_type2_text'] ?? 'Təklif '}}</span></label>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {!! Form::textarea('message', null, ["placeholder" => $dictionary['message'],'required' => 'required']) !!}
        {{ Form::hidden('method', 'feedback') }}
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
    </div>
</div>
{!! Form::close() !!}