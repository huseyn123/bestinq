@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.breadcrumbs')

    <div class="container">
        <div class="row">
            <nav id="left_navi" class="col-md-3">
                @include('web.elements.page_navbar')
            </nav>
            <div class="col-md-9 cbr_pr">

                @foreach($page->children as $block)
                    <div class="item">
                        {!! $block->content !!}
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection