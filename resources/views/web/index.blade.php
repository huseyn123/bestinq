@extends ('layouts.web', ['page_heading' => null,'index_page'=>true])

@section ('content')


    @include('web.elements.slider')


    <div class="home_bg">
        <div class="container">
            <h2 class="page_title">{{$dictionary['products'] ?? 'Məhsullar'}}</h2>
            <div class="tabs tabs1" data-target=".product_list_tab">

                @foreach($product_page as $pm)
                        <a @if ($loop->first) class="active" @endif >{{$pm->name}}</a>
                @endforeach

            </div>

            <div class="product_list_tab tabs_content">

                @foreach($product_page as $pm)
                    <div class="row @if($loop->first) active @endif product_list" data-match-height=".item">
                        @foreach($products as $product)

                            @if($product->page_id == $pm->tid)

                                <div class="col-md-3">
                                    <a class="item" href="{{route('showPage',[$product->page_slug,$product->slug])}}">
                                        @if(substr($product->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                            <span class="img"><img  src="{{$product->getFirstMedia()->getFullUrl()}}"/></span>
                                        @else
                                            <span class="img"><img  src="{{$product->getFirstMedia()->getUrl('blade')}}"/></span>
                                        @endif
                                        <span class="title">{{$product->name}}</span>
                                        <span class="desc">{{$product->summary}}</span>
                                        <span class="link"><span>{{$dictionary['read_more']}}</span></span>
                                    </a>
                                </div>

                            @endif

                        @endforeach
                    </div>
                @endforeach

            </div>


            @if($config['video_link'])

                <div class="video_block row">
                    <div class="col-md-3">
                        <div class="title">{{$dictionary['bestinq_video']}}</div>
                        <div class="desc">
                            {{$dictionary['bestinq_video_desc']}}
                        </div>
                    </div>
                    <div class="col-md-9">
                        <a data-fancybox href="{{$config['video_link']}}"><img src="{{asset('storage/files/index/home_video.jpg')}}"></a>
                    </div>
                </div>

            @endif

        </div>
    </div>


    @if($blog_news->count())

        <div class="blog_list">
            <div class="container">
                <h2 class="page_title">{{$dictionary['blog_news'] ?? 'Bloq yazıları'}}<a href="{{route('showPage',[$blog_news->first()->page_slug])}}">{{$dictionary['see_all'] ?? 'Hamısını göstər'}}</a></h2>
                <div class="row" data-match-height=".item">
                    @include('web.elements.news-grid',['data' => $blog_news])
                </div>
            </div>
        </div>
    @endif

    @if($articles->count())

        <div class="blog_list">
            <div class="container">
                <h2 class="page_title">{{$dictionary['news'] ?? 'Xəbərlər'}}<a href="{{route('showPage',[$articles->first()->page_slug])}}">{{$dictionary['see_all'] ?? 'Hamısını göstər'}}</a></h2>
                <div class="row" data-match-height=".item">
                    @include('web.elements.news-grid',['data' => $articles])
                </div>
            </div>
        </div>
    @endif


    @include('web.elements.partners')

@endsection

