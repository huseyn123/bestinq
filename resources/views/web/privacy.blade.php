@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>


    <div class="container text_page">
        <h1 class="title">{{$page->name}}</h1>

        @foreach($blocks as $block)
            <h3 class="title3">{{$block->name}}</h3>
            {!! $block->content !!}

        @endforeach

    </div>



@endsection