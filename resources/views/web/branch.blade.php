@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>

    <div class="container our_coverage">

        <div class="page_title">{{$page->name}}</div>
        <div class="form">
            @include('web.form.branch')
        </div>

        <div class="row" data-match-height=".item">

            @foreach($branches as $branch)

                <div class="col-md-4">
                    <div class="item">
                        <p><b>Ünvan:</b> {{$branch->location}}</p>
                        @if($branch->phone)<p><b>Əlaqə:</b> {{$branch->phone}}</p>@endif
                    </div>
                </div>

            @endforeach

        </div>

        <!-- Pagination Begin -->
            {!! $branches->appends(request()->input())->links('vendor.pagination.news') !!}
        <!-- Pagination End -->

    </div>

@endsection