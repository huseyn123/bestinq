@extends ('layouts.web', ['page_heading' => $dictionary['search_page'] ?? 'Axtarışın nəticələri'])

@section ('content')

    <ol id="breadcrumb" class="container" itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="/"><span itemprop="name">Ana səhifə</span></a>
            <span itemprop="position" content="1"><img src="images/breadcrumb_arrow.svg" width="7"></span>
        </li>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="/markets.html"><span itemprop="name">Marketlər</span></a>
            <span itemprop="position" content="2"></span>
        </li>
    </ol>

@endsection