@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>


    <div class="container {{config('config.kiv_class.'.$type)}}">
        <h1 class="page_title">{{$dictionary[$type.'_seo_title'] ?? ''}}</h1>

        <div class="tabs tabs1">

            @foreach(config('config.kiv_types') as $key => $value)

                <a @if($type == $key) class="active" @endif href="{{route('showPage',[$page->slug])}}?type={{$key}}">{{ $dictionary['k_'.$key] ?? $value}}</a>

            @endforeach

        </div>

        @include('web.kiv.kiv-'.$type)


    </div>

@endsection
