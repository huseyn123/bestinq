@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.breadcrumbs')

    <div class="branches container">
        <ul id="top_navi" class="tabs">
            @include('web.elements.shop_navbar',['li'=> true])
        </ul>

        <div class="tabs_content">
            <div class="active">
                <div class="row">
                    @foreach($blocks as $block)
                        <div class="col-md-4">
                            <div class="item">
                                <h4 class="title">{{ $block->name }}</h4>
                                {!! $block->content !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>

@endsection