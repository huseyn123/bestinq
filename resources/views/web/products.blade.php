@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>

    <div class="container">
        <h1 class="page_title">{{$page->seo_title}}</h1>
        <div class="product_list_tab tabs_content">
            <div class="row active product_list" data-match-height=".item">

                @include('web.elements.product_crud')

            </div>

        </div>

    </div>
@endsection