@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>

    <div class="faq_page container">
        <h1 class="page_title">{{$page->seo_title}}</h1>
        @foreach($faqs as $faq)
            <div class="item">
                <h3>{{ $faq->question }}   <i class="fa fa-angle-down"></i></h3>

                <div><p>{{ $faq->answer }}</p></div>
            </div>
        @endforeach

    </div>

@endsection