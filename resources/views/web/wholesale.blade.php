@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>


    <div class="container partner_page">
        <h1 class="page_title">{{$page->seo_title}}</h1>
        <div class="row">
            <div class="col-md-4">
                <div class="form main_wr_id">
                    @include('web.elements.succes_form')
                    @include('web.form.wholesale',['id' =>'wr_id'])
                </div>
                <a class="download_btn">{{$dictionary['catalogs'] ?? 'Elektron Məhsul Kataloqu'}}</a>
            </div>
            <div class="col-md-8">
                <div class="cart">
                    <div class="title">{{$dictionary['conditions'] ?? 'Şərtlər'}}</div>
                    <div class="tabs tabs1" data-target=".tabs_content">

                        @foreach($blocks as $block)
                            <a @if($loop->first) class="active" @endif>{{$block->name}}</a>
                        @endforeach

                    </div>
                    <div class="tabs_content">

                        @foreach($blocks as $block)
                            <div class="text @if($loop->first) active @endif">
                                {!! $block->content !!}
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection