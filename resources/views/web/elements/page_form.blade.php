<div class="page_form @if(isset($class)) {{$class}} @endif">
    <div class="page_form_inner">
        <h1>{{ $dictionary['page_form'] ?? 'Müraciət Forması'}}</h1>

        {!! Form::open(['url'=>route('web.service_form'), 'method'=>'POST', 'id' => 'service_form']) !!}

            <div class="item">
                {!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'class' => 'alphaonly ipt_style']) !!}
            </div>
            <div class="item">
                {!! Form::text('email', null, ["placeholder" => '*'.$dictionary['email'],'class' => 'ipt_style']) !!}
            </div>
            <div class="item">
                {!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'class' => 'ipt_style phonenumber']) !!}
            </div>

            <div class="item">
                {!! Form::hidden('service_name',$page->name) !!}
            </div>

            <div class="item">
                {!! Form::textarea('text', null, ["placeholder" => $dictionary['message'] ,'class' => 'ipt_style']) !!}
            </div>
            <div class="clearfix">
                {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
            </div>

        {!! Form::close() !!}

    </div>
    @include('web.elements.succes_form',['id' => 'success_service_form'])

</div>