<div class="active">
    @foreach($reports as $report)
        <div class="col-md-4">
            @if($report->getFirstMedia())
                <a class="item" href="{{ $report->getFirstMedia()->getFullUrl() }}" download="download">
                    {{ $report->name }}
                </a>
            @endif
        </div>
    @endforeach
</div>