<header id="header">
    <div class="top">
        <div class="container">
            <ul class="contact">
                <li>{{$dictionary['address']}}</li>
                <li>{{$config['contact_phone']}}</li>
                <li>{{$config['contact_email']}}</li>
            </ul>
            <div class="search">
                <div class="input">
                    @include('web.elements.search')
                </div>
                <button class="toggle"><i class="icofont-search-1"></i></button>
            </div>

            <div class="lang">
                @include('web.elements.lang')
            </div>
        </div>
    </div>
    <div class="inner container">
        <div>
            <div>
                <button class="open_mobile_menu"></button>
                <div class="logo"><a href="{{route('home')}}"><img src='{{asset("images/logo.svg")}}'/></a></div>
            </div>
            <nav>
                <button class="close_mobile_menu"></button>
                @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,3,4],'type' => 'header'])
            </nav>
        </div>
    </div>
</header>