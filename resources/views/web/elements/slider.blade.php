@if($sliders->count())
    <div id="home_slider" class="owl-carousel">
        @foreach($sliders as $slider)

            <div class="item"  @if($slider->getFirstMedia('mobile')) style="background-image: url('{{$slider->getFirstMedia('mobile')->getUrl('view')}}')" @endif>

                @if($slider->getFirstMedia())
                    <img src="{{$slider->getFirstMedia()->getUrl('view')}}" />
                @endif

                <div>
                    <div class="title1">{{$slider->title}}</div>
                    <div class="title2">{{$slider->title2}}</div>
                    <div class="desc">{{$slider->summary}}</div>
                    @if($slider->link)
                        <a href="{{$slider->link}}" target="_blank">{{$dictionary['read_more']}}</a>
                    @endif
                </div>
            </div>

        @endforeach
    </div>
@endif