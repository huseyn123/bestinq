@if($similars->count())

    <section class="product_section pt0">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- HeadBlock Begin -->
                    <div class="head_block clearfix">
                        <h1>Digər oxşar müraciətlər</h1>
                    </div>
                    <!-- HeadBlock End -->
                    <!-- ProductList Begin -->
                    <div class="product_list row">

                        @foreach($similars as $similar)

                            <div class="col-md-3 col-xs-6 col-mob-12">
                                <div class="box">
                                     @if($similar->getFirstMedia())
                                        <figure>
                                            <img src="{{ asset($similar->getFirstMedia()->getUrl('blade')) }}" alt="{{$similar->name}}">
                                        </figure>
                                    @endif

                                    <div class="body">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ percentage($similar->amount,$similar->done_amount) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ percentage($similar->amount,$similar->done_amount) }}%"></div>
                                            <span>{{ percentage($similar->amount,$similar->done_amount) }}%</span>
                                        </div>
                                        <span class="price">{{  $similar->done_amount }} <span>AZN</span></span>
                                        <h2>Əli Əliyev</h2>
                                        <ul class="list-unstyled">
                                            <li><span>Kateqoriya:</span>{{  $similar->page_name }}</li>
                                            <li><span>Məbləğ:</span>{{  $similar->amount }} AZN</li>
                                            <li><span>Son tarix:</span>{{ blogDate($similar->end_date )}}</li>
                                        </ul>
                                        <span class="more"><a href="{{ route('showPage',[$similar->page_slug,$similar->slug]) }}" title="">Ətraflı bax</a></span>
                                    </div>
                                </div>
                            </div>

                        @endforeach

                    </div>
                    <!-- ProductList End -->
                </div>
            </div>
        </div>
    </section>


@endif