@foreach($data as $article)

    <div class="col-md-4">
        <a class="item" href="{{ route('showPage',[$article->page_slug,$article->slug]) }}">
            @if($article->getFirstMedia())
                <img src="{{ asset($article->getFirstMedia()->getUrl('blade')) }}"/>
            @endif
            <span class="title">{{ $article->name }}</span>
            <span class="desc">{{ $article->summary }}</span>
            <span class="dtl">
               <span class="date"><i class="fa fa-calendar"></i> {{ blogDate($article->published_at) }}</span>
                {{--<span>{{$dictionary['read_more'] ?? 'Ətraflı oxu'}}</span>--}}
            </span>
        </a>
    </div>

@endforeach


