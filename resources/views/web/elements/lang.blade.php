<span>{{ strtoupper($lang) }}<i class="fa fa-angle-down"></i></span>
<div>
    @foreach(config("app.locales") as $key => $locale)
        @if($key != $lang)
            <a  href="{{ url($relatedPages[$key] ?? $key) }}" >{{ strtoupper($key) }}</a>
        @endif
    @endforeach
</div>

