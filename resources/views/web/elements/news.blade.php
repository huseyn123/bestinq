@foreach($articles as $article)
    <div class="content col-xs-6 @if($loop->iteration%2 != 0) clearfix @endif">
        <article>
            <div class="post-image">
                {{ $article->getFirstMedia() }}
                <div class="category"><a href="{{ route('showPage', $article->page_slug) }}">{{ $article->page_name }}</a></div>
            </div>
            <div class="post-text">
                <span class="date">{{ blogDate($article->published_at) }}</span>
                <h2><a href="{{ route('showPage', [$article->page_slug, $article->slug]) }}">{{ $article->name }}</a></h2>
            </div>
        </article>
    </div>
@endforeach