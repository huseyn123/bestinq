<script>
    $('.project_wrapper .project_years a').click(function (e) {
        var active = $(this).hasClass('active');
        if(!active){
            $('.project_wrapper .project_years a').removeClass('active');
        }
    });

    var numb = {{ request()->get('page') ?: 1 }};
    $('#projectsLoadingButton').click(function () {
        numb++;
        $.ajax({
            url: '{{ route('showPage',[$page->slug]).'?year='.$selectedYear }}'+'&page='+numb,
            beforeSend: function(response) {
                $('#projectsLoadingButton').button('loading');
            }
        })
            .done(function(response) {
                $('.project_list .productList').append(response.html);
                if(response.projects.to == response.projects.total){
                    $('#projectsLoadingButton').remove();
                }else{
                    $('#projectsLoadingButton').button('reset');
                }
            })
            .fail(function(xhr, ajaxOptions, thrownError) {
                requestSent = false;
                var json = JSON.parse(xhr.responseText);
            });
    })

</script>
