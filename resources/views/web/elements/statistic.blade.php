@if(isset($about))

    <div class="about_block clearfix">
        <div class="about_block_inner">
            <div class="block">
                <span class="count">{{ $config['experience_count'] }}</span>
                <span class="text">{{ $dictionary['statistic_experience'] }}</span>
            </div>
            <div class="block">
                <span class="count">{{ $config['service_count'] }}</span>
                <span class="text">{{ $dictionary['service_experience'] }}</span>
            </div>
            <div class="block">
                <span class="count">{{ $config['project_count'] }}</span>
                <span class="text">{{ $dictionary['project_experience'] }}</span>
            </div>
        </div>
    </div>


@else

    <section class="count_section">
    <div class="container">
        <div class="row">
            <div class="col-xs-4">
                <div class="box">
                    <span class="count">{{ $config['experience_count'] }}</span>
                    <span class="txt">{{ $dictionary['statistic_experience'] }}</span>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <span class="count">{{ $config['service_count'] }}</span>
                    <span class="txt">{{ $dictionary['service_experience'] }}</span>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <span class="count">{{ $config['project_count'] }}</span>
                    <span class="txt">{{ $dictionary['project_experience'] }}</span>
                </div>
            </div>
        </div>
    </div>
</section>
@endif