<div class="container">
    <div class="brends_carousel">
        <div class="owl-carousel">

            @foreach($partners as $partner)
                @if($partner->getFirstMedia())
                    <a class="item"><img src="{{ asset($partner->getFirstMedia()->getUrl()) }}"/></a>
                @endif
            @endforeach

        </div>
    </div>
</div>