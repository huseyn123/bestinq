@if($similarArticles->count())
        <h3 class="title">{{ $page->template_id == 3 ? $dictionary['similar_news'] : $dictionary['similar_blogs']}}</h3>
        @foreach($similarArticles as $similarArticle)
            <a href="{{ route('showPage',[$similarArticle->slug,$similarArticle->article_slug]) }}"><span>{{ blogDate($similarArticle->published_at) }}</span> {{ $similarArticle->name }} </a>
        @endforeach
@endif

