<!-- MobileNav Begin -->
<div class="mobile_nav">
    <div class="head clearfix">
        <span class="close"></span>
    </div>
    <div class="body">
        @include('web.elements.menu', ['parent' => null, 'lang' => $lang, 'hidden' => [0,1,2,5], 'sub' => true, 'type' => 'mobile'])
        <a href="tel:1111" title="" class="call_center">{{ $config['hotline'] }}</a>
        <a href="#" title="" class="call_back open_popup" data-type="call">{{ $dictionary['call_back'] ?? 'Geri zəng'}}</a>
    </div>
</div>
<!-- MobileNav Begin -->