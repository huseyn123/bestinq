@if(isset($page))

    <div class="page_header">
        @if($page->getFirstMedia('cover'))
            <img src="{{$page->getFirstMedia('cover')->getUrl('blade')}}"/>
        @endif
        <div>{{$page->name}}</div>
    </div>

@endif
