<footer id="footer">
    <nav>
        <ul class="container">

            @foreach($menu as $m)
                @if(($m->parent_id === null) && ($m->lang == $lang) && ($m->menu_children_count > 0) && (in_array($m->visible, [1,3])))
                    <li>
                        <a>{{$m->name}}</a>
                        <div>
                            @include('web.elements.menu', ['parent' => $m->tid,'sub' => true,'lang' => $m->lang, 'hidden' => [0,2,4],'type' => 'footer'])
                        </div>
                    </li>
                @endif
            @endforeach


            <li>
                @foreach($menu as $m)
                    @if(($m->parent_id === null) && ($m->lang == $lang) && ($m->menu_children_count == 0) && (in_array($m->visible, [1,3])))
                        <a href="{{route('showPage',[$m->slug])}}">{{$m->name}}</a>
                    @endif
                @endforeach

                <div class="social">
                    @include('web.elements.social')
                </div>
            </li>

        </ul>
    </nav>
    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-3">{{ $dictionary['copyright'] }}. © {{date('Y')}}</div>
                @foreach($menu as $m)

                    @if(($m->parent_id === null) && ($m->lang == $lang)&& ($m->menu_children_count == 0) && (in_array($m->visible,[4])) )
                        <div class="col-md-3"><a href="{{route('showPage',[$m->slug])}}">{{$m->name}}</a></div>
                    @endif

                @endforeach

                <div class="col-md-3">{!! trans('locale.site_by', ['site' => '<a class="marcom" href="http://marcom.az" target="_blank">Marcom</a>']) !!} </div>
            </div>
        </div>
    </div>
</footer>
