@if($type == 'header' || $type == 'mobile')
    <ul>
        @foreach($menu as $m)
            @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang == $lang && !in_array($m->template_id,[0]))
                <li>
                    <a href="@if(in_array($m->template_id,[1])) JavaScript:Void(0);  @else {{route('showPage',[$m->slug])}} @endif"  title="{{ $m->name }}">{{ $m->name}} @if(in_array($m->template_id,[1]) && $sub == true)<i class="fa fa-angle-down"></i> @endif</a>
                    @if($sub == true && in_array($m->template_id,[1]))
                        @include('web.elements.menu',['type' => $type,'parent' => $m->tid,'template_id' => $m->template_id,'sub' => false,'hidden' =>[0,3,4]])
                    @endif
                </li>
            @endif
        @endforeach
    </ul>

@elseif($type == 'footer')
    @foreach($menu as $m)
        @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang == $lang)
            <a href="{{ route('showPage',[$m->slug]) }}"  title="{{$m->name}}">{{$m->name}}</a>
        @endif
    @endforeach
@endif