@if($blocks->count())
    <div class="container about">
    <div class="row">

        @foreach($blocks->chunk(2) as $chunk)
            <div class="col-sm-6">
                @foreach($chunk as $block)
                    <div class="item">
                        <div><h4>{{ $block->name }}</h4></div>
                        {!! $block->content !!}
                    </div>
                @endforeach
            </div>
        @endforeach

    </div>
</div>
@endif