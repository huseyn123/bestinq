@foreach($products as $product)

    <div class="col-md-3">
        <a class="item" href="{{route('showPage',[$product->page_slug,$product->slug])}}">
            @if($product->getFirstMedia())
                @if(substr($product->getFirstMedia()->getFullUrl(), -3) == 'svg')
                    <span class="img"><img  src="{{$product->getFirstMedia()->getFullUrl()}}"/></span>
                @else
                    <span class="img"><img  src="{{$product->getFirstMedia()->getUrl('blade')}}"/></span>
                @endif
            @endif
            <span class="title">{{$product->name}}</span>
            <span class="desc">{{$product->summary}}</span>
            <span class="link"><span>{{$dictionary['read_more']}}</span></span>
        </a>
    </div>

@endforeach