@foreach($pages as $page)

    <div class="box">
        <h2>{{ $page->name }}</h2>
        @if($page->content) {!! $page->content !!} @elseif($page->summary) <p>{{ $page->summary }}</p> @endif
        <span class="more"><a href="{{ route('showPage',[$page->slug]) }}">{{ $dictionary['read_more'] }}</a></span>
    </div>

@endforeach