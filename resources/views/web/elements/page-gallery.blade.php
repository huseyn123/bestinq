@if(!is_null($page->album_id))

    <div class="gallery">
        @foreach($page->getMedia('gallery')->chunk(ceil(count($page->getMedia('gallery'))/3)) as $gallery)
            <div class="item-gallery-{{ config('config.gallery-side.'.$loop->iteration) }}">
                @foreach($gallery as $img)
                    @if($loop->parent->iteration == 3)
                        <img src="{{ $img->getUrl() }}">
                    @else
                        <img src="{{ $img->getUrl('small') }}">
                    @endif
                @endforeach
            </div>
        @endforeach

        <div class="clearfix"></div>
    </div>

@endif

@push('scripts')
    <script>
        $(".item-gallery-center").insertAfter(".item-gallery-left");
    </script>
@endpush