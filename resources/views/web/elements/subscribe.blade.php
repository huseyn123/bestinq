{!! Form::open(['url'=>route('subscribe'), 'method'=>'POST', 'id' => 'subscribe-form']) !!}

    {!! Form::text('email', null, ['placeholder' => $dictionary['enter_email'], 'required' => 'required']) !!}
    {!! Form::button($dictionary['subscribe_b'] ?? '', ['id' => 'loadingSubButton', 'data-loading-text' => loading(), 'autocomplete' => 'off', 'type' => 'submit']) !!}
    <p id="subscribeBox"></p>

{!! Form::close() !!}
