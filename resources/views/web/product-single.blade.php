@extends ('layouts.web', ['page_heading' => $product->name,'other_page'=>$product] )

@section ('content')

    @include('web.products.cover')

    <div class="container">
        @include('web.elements.breadcrumbs',['data' => $product])
    </div>


    <div class="container qennadi_miksler">
        <div class="pro_block17">
            {!! $product->content !!}
        </div>

        <div class="row pro_group1">
            <div class="col-md-6">

                @if($product->getMedia('gallery'))

                    <div class="pro_block18">
                        <div class="row">
                            @foreach($product->getMedia('gallery') as $gallery)
                                <div class="col-md-6"><div class="pro_img1"><img src="{{asset($gallery->getUrl('blade'))}}" alt="{{$product->name}}" title="{{$product->image_meta_title}}"></div></div>
                            @endforeach
                        </div>
                    </div>

                @endif

            </div>

            <div class="col-md-6">
                @include('web.form.product_single',['id'=>'product_id','p_name'=>$product->name])
            </div>

        </div>
    </div>


    <div class="other_product">
        <div class="container">
            <h2 class="page_title">{{$dictionary['other_products'] ?? 'Digər Məhsullar'}}</h2>
            <div class="row product_list" data-match-height=".item">

                @foreach($similarProducts as $sm)

                    <div class="col-md-3">
                        <a class="item" href="{{route('showPage',[$sm->page_slug,$sm->slug])}}">

                            @if($sm->getFirstMedia())
                                @if(substr($sm->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                    <span class="img"><img  src="{{$sm->getFirstMedia()->getFullUrl()}}"/></span>
                                @else
                                    <span class="img"><img  src="{{$sm->getFirstMedia()->getUrl('blade')}}"/></span>
                                @endif
                            @endif

                            <span class="title">{{$sm->name}}</span>
                            <span class="desc">{{$sm->summary}}</span>
                            <span class="link">{{$dictionary['read_more']}}</span>

                        </a>
                    </div>

                @endforeach

            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $('.pro_block17 ul').addClass('col-md-6');
    </script>
@endpush