@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <div class="container">
        @include('web.elements.breadcrumbs')
    </div>




    <div class="container text_page">

        <h2 class="title">{{$page->name}}</h2>

        @foreach($blocks as $block)
            <h3 class="title3">{{$block->name}}</h3>
            {!! $block->content !!}

            @if($blocks->count() > 0 && !$loop->last)
                <hr class="dashed"/>
            @endif

        @endforeach



    </div>


@endsection