@extends ('layouts.web', [
'page_heading' => Cache::get('dictionary_'.app()->getLocale())['search_result_title'],
'menuWithoutSlug' => true,
'menu' => \App\Logic\Menu::all(),
'config' => \App\Logic\WebCache::config()->pluck('value', 'key'),
'dictionary' => Cache::get('dictionary_'.app()->getLocale()),
'social' => Cache::get('social'),
'lang' => app()->getLocale(),
])

@section ('content')

    <div class="container">

        <ol id="breadcrumb" class="container" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="{{ route('home') }}"><span itemprop="name">Ana səhifə</span></a>
                <span itemprop="position" content="1"><img src="{{asset('images/breadcrumb_arrow.svg')}}" width="7"></span>
            </li>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" ><span itemprop="name">{{ $dictionary['search_result_title'] }}</span></a>
                <span itemprop="position" content="2"></span>
            </li>
        </ol>

    </div>


    <!-- ApartmentList Begin -->
    <section class="apartment_list mb40">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="search_result">
                        <span><strong>Axtarış Nəticəsi: </strong> {{ $apartments->count() }} nəticə tapıldı</span>
                    </div>
                </div>
                @include('web.elements.apartment-list')
            </div>
        </div>
    </section>
    <!-- ApartmentList End -->


    <div class="search_result container">
        <h2 class="title">Axtarışın Nəticəsi: <span>4 nəticə tapılıb</span></h2>
        <div class="item">
            <h3>Ramazan ayı Rahat Marketdə</h3>
            <p>Rahat Marketdə müştərilərinə bu istəklərini reallaşdıra bilməkləri üçün əla fürsət yaradır. Rahat Marketdə müştərilərinə bu istəklərini reallaşdıra bilməkləri
                üçün əla fürsət yaradır. Rahat Marketdə müştərilərinə bu istəklərini reallaşdıra bilməkləri üçün əla fürsət yaradır. Rahat Marketdə müştərilərinə bu...</p>
            <a>Ardını oxu</a>
        </div>
        <div class="item">
            <h3>Ramazan ayı Rahat Marketdə</h3>
            <p>Rahat Marketdə müştərilərinə bu istəklərini reallaşdıra bilməkləri üçün əla fürsət yaradır. Rahat Marketdə müştərilərinə bu istəklərini reallaşdıra bilməkləri
                üçün əla fürsət yaradır. Rahat Marketdə müştərilərinə bu istəklərini reallaşdıra bilməkləri üçün əla fürsət yaradır. Rahat Marketdə müştərilərinə bu...</p>
            <a>Ardını oxu</a>
        </div>
        <div class="item">
            <h3>Ramazan ayı Rahat Marketdə</h3>
            <p>Rahat Marketdə müştərilərinə bu istəklərini reallaşdıra bilməkləri üçün əla fürsət yaradır. Rahat Marketdə müştərilərinə bu istəklərini reallaşdıra bilməkləri
                üçün əla fürsət yaradır. Rahat Marketdə müştərilərinə bu istəklərini reallaşdıra bilməkləri üçün əla fürsət yaradır. Rahat Marketdə müştərilərinə bu...</p>
            <a>Ardını oxu</a>
        </div>
    </div>

@endsection