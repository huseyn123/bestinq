@extends ('layouts.web', ['page_heading' => $project->name] )

@section ('content')

    @include('web.elements.breadcrumbs',['data' => $project])


    <!-- ProjectDetail Begin -->
    <section class="project_detail">
        <div class="container">
            <div class="row">
                @if($project->getMedia('gallery')->count())

                    <div class="col-md-6 col-xs-12">
                        <!-- ProductGallery Begin -->
                        <div class="product_gallery">
                            <div class="stage">
                                <div class="carousel-stage">
                                    <ul class="list-unstyled">
                                        @foreach($project->getMedia('gallery') as $gallery)
                                            <li><img src="{{ $gallery->getUrl('blade') }}"></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- ProductGallery End -->
                    </div>
                @endif
                <div class="col-md-6 hidden-sm hidden-xs">
                    <div >
                        <h1 class="title">{{ removeSymbol($project->name,'/') }}</h1>
                        <div class="text">
                            {!! $project->content !!}
                        </div>
                    </div>
                    <div class="call_back_form">
                        @include('web.elements.detail-form')
                    </div>
                </div>
                @if($project->getMedia('gallery')->count())
                    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="navigation">
                    <div class="carousel-navigation">
                        <ul class="list-unstyled">
                            @foreach($project->getMedia('gallery') as $gallery)
                                <li><img src="{{ $gallery->getUrl('thumb') }}"></li>
                            @endforeach
                        </ul>
                    </div>
                    <a href="#" class="prev prev-stage"></a>
                    <a href="#" class="next next-stage"></a>
                </div>
            </div>
                @endif
                <div class="col-xs-12 visible-sm visible-xs mt30">
                    <div >
                        <h1 class="title">{{ removeSymbol($project->name,'/') }}</h1>
                        <div class="text">
                            {!! $project->content !!}
                        </div>
                    </div>
                    <div class="call_back_form">
                        @include('web.elements.detail-form')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ProjectDetail End -->

    @include('web.elements.similarProjects')

@endsection
