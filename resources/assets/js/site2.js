

$(function() {

    $.mask.definitions['9'] = '';
    $.mask.definitions['d'] = '[0-9]';
    $(".phonenumber").mask("+(994 dd) ddd dd dd");

});


$("#subscribe-form").on("submit",function (event){

    event.preventDefault();
    subscribe($(this));
});

$('.report .tabs a').click(function () {
    var route = $(this).attr('href');

    $.ajax({
        url: route,
        beforeSend: function(response) {
        }
    })
    .done(function(response) {
        $('.report  .tabs_content').html(response.html);
    })
    .fail(function(xhr, ajaxOptions, thrownError) {
        requestSent = false;
        var json = JSON.parse(xhr.responseText);
    });
});


function subscribe(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            data: action.serialize(),
            dataType: "json",

            beforeSend: function(response) {
                $("#loadingSubButton").button('loading');
            },

            success: function(response){
                $("#loadingSubButton").button('reset');

                if(response.code == 0) {

                    $('#subscribe-form')[0].reset();
                    setTimeout(function(){ $("#subscribeBox").html('');}, 3000);
                }

                $("#subscribeBox").html(response.msg);
            },

            error: function(res){
                $("#loadingSubButton").button('reset');
                $("#subscribeBox").html('<p>Unexpected Error!</p>');
            }
        });
    }
}



$(function(){

    function preventNumberInput(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode > 47 && keyCode < 58) {
            e.preventDefault();
        }
    }
    function preventOnlyNumberInput(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode > 31 && (keyCode < 48 || keyCode > 57)){
            e.preventDefault();
        }
    }

    $('.alphaonly').keypress(function(e) {
        preventNumberInput(e);
    });

    $('.alphaonlynumb').keypress(function(e) {
        preventOnlyNumberInput(e);
    });



});

//Rezervation
function g_form(action,type){
    $.ajax({
        url:  action.attr('action'),
        type: action.attr('method'),
        data: new FormData(action[0]),
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function(response) {
            $(".msg").html('');
            $("#loadingButton").button('loading');
        },

        success: function(response){
            $("#loadingButton").button('reset');

            if(response.code == 1) {

                $('button[type=submit]').attr('disabled',true);

                action.data('submitted', true);
                $(".msg").html(response.msg);
            }
            else{
                $("#"+type)[0].reset();
                $(".msg").html('<i class="fa fa-check" aria-hidden="true"></i>'+response.msg);
            }

        },

        error: function(res){
            $("#loadingButton").button('reset');
            $(".msg").html('Unexpected Error!');
        }
    });
}
$("#product_form").on("submit",function (event){

    event.preventDefault();
    MainForm($(this),'product_form');
});

$("#st_id").on("submit",function (event){

    event.preventDefault();
    MainForm($(this),'st_id');
});

$(".main_f").on("submit",function (event){
    var id=$(this).attr('id');
    event.preventDefault();
    MainForm($(this),id);
});

function MainForm(action,type)  {
    $.ajax({
        url:  action.attr('action'),
        type: action.attr('method'),
        data: new FormData(action[0]),
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function(response) {
            $("#"+type+"_message").html('');
        },

        success: function(response){

            if(response.code == 1) {
                $("#"+type+" span.error_message").html(response.msg);
            }
            else{
                $('#'+type)[0].reset();
                $('.main_'+type).addClass('success');
                $('.s_form').show();
            }
        },

        error: function(res){
            $("#"+type+" .error_message").html('Unexpected Error!');
        }
    });
}






