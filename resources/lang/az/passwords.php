<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Şifrə ən azı 6 simvoldan ibarət olmalıdır.',
    'reset' => 'Şifrəniz bərpa edildi!',
    'sent' => 'Şifrənizin bərpası üçün e-poçtunuzu yoxlayın!',
    'token' => 'This password reset token is invalid.',
    'user' => "E-poçta uyğun istifadəçi tapılmadı.",

];
