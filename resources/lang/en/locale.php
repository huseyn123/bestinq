<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 5/11/18
 * Time: 19:40
 */

return [
    'create' => 'Yeni',
    'cancel' => 'Bağla',
    'save' => 'Yadda saxla',
    'add' => 'Əlavə et',
    'action' => 'Əməliyyat',
    'active' => 'Aktiv',
    'trash' => 'Slinən',
    "schedule_day" => "G\u00fcn",
    "schedule_week" => "Həftə",
    "schedule_month" => "Ay",
    "schedule_list" => "Siyahı",
    "page" => "Səhifə",
    "config" => [
        'sex' => ['Female', 'Male'],
        "minDates" => ['Baz', 'B.e', 'Ç.a', 'Çər', 'C.a', 'Cüm', 'Şən'],
        "dates" => ['Bazar günü', 'Bazar ertəsi', 'Çərşənbə axşamı', 'Çərşənbə', 'Cümə axşamı', 'Cümə', 'Şənbə'],
        "months" => ['Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'İyun', 'İyul', 'Avqust', 'Sentyabr', 'Oktyabr', 'Noyabr', 'Dekabr'],
        "minMonths" => ['Yan', 'Fev', 'Mar', 'Apr', 'May', 'İyn', 'İyl', 'Avq', 'Sen', 'Okt', 'Noy', 'Dek'],
        "eventDates" => ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'],
    ],
    'subCopy' => "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n". 'into your web browser: [:actionURL](:actionURL)',
    'site_by' => 'Site by :site',
];
