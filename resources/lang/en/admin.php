<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 4/14/18
 * Time: 21:09
 */

return [
    'login' => 'Log in',
    'login_text' => '',
    'login_remember' => 'Yadda saxla',
    'login_sign_in' => 'Daxil ol',
    'password_lost' => 'Şifrəni unutdum',
    'password_reset' => 'Reset Password',
    'password_reset_link' => 'Send Password Reset Link',
    'logout' => 'Çıxış',
    'profile' => 'Profil',
];
